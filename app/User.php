<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    //عملنا فانيكشن هنا علشان نعرف المودل ان جدول الجندر واليوزرم بينهم علاقة
    public function gender() {       // البريمارى كاى          الفورن كاى            المودل اللى انا هروحله بتاع جدول الجندر
        return $this->belongsTo(\App\Gender::class, 'gender_id', 'id');
    }

    //عملنا فانيكشن هنا علشان نعرف المودل ان جدول اليوزر وجدول الصور  بينهم علاقة
    public function image() {       // البريمارى كاى          الفورن كاى            المودل اللى انا هروحله بتاع جدول الجندر
        return $this->belongsTo(\App\Image::class, 'image_id', 'id');
    }


    //عملنا فانيكشن هنا علشان نعرف المودل ان جدول الشات واليوزرم بينهم علاقة
    public function sentMessages() {      // البريمارى كاى          الفورن كاى            المودل اللى انا هروحله
        return $this->hasMany(\App\Chat::class, 'first_user_id', 'id');    //دى الشاتات اللى انا باعتها
    }


      //عملنا فانيكشن هنا علشان نعرف المودل ان جدول الشات واليوزرم بينهم علاقة
    public function receivedtMessages() {      // البريمارى كاى          الفورن كاى            المودل اللى انا هروحله
        return $this->hasMany(\App\Chat::class, 'second_user_id', 'id');   //دى الشاتات اللى انا مستقبلهاا

    }

}









