<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HolderController extends Controller
{
    public function getUploadFileView(){
        return view('tests.upload-file');
    }




    public function doUploading(Request $request){

        //هات الصورة من الفايل وحطها فى متغير
        $f = $request->file('f');



        //عملنا اسوشيتف اراى علشان نحط جواها الحاجات الفالديشن المطلوبة على كل صورة بنرفعها
        $rules = [
            //بنقولة الصورة مطلوبة وتكون اكبر صورة حجمها ميتعداش ال2 ميجا وانواعها اللى مكتوبة دى
          'f' => 'required|max:2048|mimes:jpg,jpeg,png'
        ];


        // بنعمل متغير اسمه فلاديتور بياخد خصائص الفلاديتور ويعدين بنديله البيانات اللى عايزينه يعمل عليها وبنديله القواعد
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {           //بنقولة لو الفالديشن فشل
            return redirect('/tests/upload-file') //حولة على صفحة الريجستريشن مرة تانيه
                ->WithErrors($validator->errors()); // وبعدين طلعله الايرور
        }


     $ext = $f->getClientOriginalExtension();  //علشان اجيب الاكستنشن بتاعها او الامتداد

      $name = $f->getClientOriginalName();     //لو عايز اجيب اسم الصورة الحقيقى

        // شفر الوقت علشان الاسم ميتكررش وضيف عليه الاكستنشن علشان يطلع نقطة png  او   jpg وعلشان يطلع باسم جديد
        $newname = sha1(time()) . "." . $ext;

        //بنقولة خزن فى الفولدر اللى اسمه public واحفظ الصورة بالاسم الجديد وهات محتوى الصورة من المتغير f
        \Storage::disk('public')->put($newname, \File::get($f));

    }

}
