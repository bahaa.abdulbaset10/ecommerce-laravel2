<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{
    //الفانيكشن بتاعت كارت الشراء
    public function index(){

        $lastOrder= \App\Order::where('is_checked_out', '=',false) //هات اخر اوردر من جدول الاوردرات للمستخدم ده
        ->where('user_id', '=', \Auth::user()->id)   //بشرط اليوزر اى دى اللى فى جدول الاورد يساوى الاى دى للمستخدم اللى عامل لوجين
        ->orderBy('created_at', 'DESC')->first();   //ورتبهم تنازلى من الكبير الى الصغير حسب الانشاء بتاعهم وهات اول واحد


     //لو فيه اوردر يتعرض ولو مفيش يعرضله رساله يقوله مفيش اوردرات
        return view('shopping-cart',[ //هات صفحة كارت التسوق وهات معاها اخر اوردر انا عامله

            //دى الفانيكشن اللى عملنها فى المودل بتاع الاوردر اللى بتجيب تفاصيل المنتج
            'lastOrderDetails' => $lastOrder ? $lastOrder->orderDetails : [], //لو اخر اوردر موجود هات التفاصيل بتاعته ولو مش موجود اعمل مصفوفة فاضية
        ]);
    }


    //الفانيكشن بتاعت مسح عنصر من كارت الشراء
    public function deleteItemFromCart(Request $request){
        //1. هنتأكد ان اليوزر عامل لوجين
        if(!\Auth::check()){  //لو المستخدم ضغط على الزرار من غير ما يعمل لوجن
            return redirect('/login');  //ارميه بره على صفجة اللوجين
        }
        //2. هنتأكد ان المنتج موجود
            //القواعد
        $rules=['product' => 'required'];   //اسم المنتج دا مطلوب product دى اللى معمولة فى الزرار المخفى فى الview بتاع الـproduct
        $validator = \Validator::make($request->all(), $rules); //هات كل البيانات والقواعد اللى انا حطتهالك تمشى عليها
        if($validator->fails()){     //هعمل فالديشن لو المنتج مش موجود يرجعه لصفحة كارت التسوق مرة تانيه
            return redirect('/shopping-cart');
        }
        $productID = $request->get('product'); //ولو ما فشلش خليه يجيب البيانات ويحطها فى المتغير اللى هو$productID

        $product = \App\Product::find($productID); //هات الاى دى بتاع المنتج من جدول المنتجات
        if(!$product) {  //لو المنتج مش موجود فى الدتا بيس او مفيش منتج
            return redirect('/shopping-cart');  //حولة على صفحة كارت التسوق
        }
        //3.هنتأكد ان الاوردر موجد
        $lastOrder= \App\Order::where('is_checked_out', '=',false) //هات اخر اوردر من جدول الاوردرات للمستخدم ده
        ->where('user_id', '=', \Auth::user()->id)   //بشرط اليوزر اى دى اللى فى جدول الاورد يساوى الاى دى للمستخدم اللى عامل لوجين
        ->orderBy('created_at', 'DESC')->first();   //ورتبهم تنازلى من الكبير الى الصغير حسب الانشاء بتاعهم وهات اول واحد
        if($lastOrder){   //لو اخر اوردر موجود
            //هات تفاصيل الاوردر من جدول الـ OrderDetails بشرط ان الاى دى بتاع المنتج يساوى اى دى المنتج اللى انا جبته
            $productInOrder = \App\OrderDetail::where('product_id', '=', $productID)
                ->where('order_id', '=', $lastOrder->id)->first();//وفى نفس الوقت الاوردر بتاع الاى دى يكون نفس الاوردر اللى انا جبته
            if ($productInOrder) { //لو الاى دى بتاع الاوردر اللى انا جبته والاى دى بتاع المنتج اللى انا جبته
                $productInOrder->delete(); //امسحه
                return redirect('/shopping-cart') //وودينى على صفحة كارت التسوق
                    ->with(['success' => 'Item is removed from your cart successfully!']); //واعمله رسالة نجاح
            } else { //لو الاى دى بتاع الاوردر اللى انا جبته والاى دى بتاع المنتج اللى انا جبته مش موجود فى الدتا بيس
                return redirect('/shopping-cart'); //حوله على صفحة كارت التسوق
            }
        }else{   //لو اخر اوردر مش موجود
            return redirect('/shopping-cart');  //حولة على صفحة كارت التسوق
        }
    }

    //الفانيكشن بتاعت مسح الاوردر كله او كنسلت الأوردر
    public function cancelOrder(Request $request)
    {

        //1. هنتأكد ان اليوزر عامل لوجين
        if (!\Auth::check()) {  //لو المستخدم ضغط على الزرار من غير ما يعمل لوجن
            return redirect('/login');  //ارميه بره على صفجة اللوجين
        }


        //2.هنتأكد ان الاوردر موجد
        $lastOrder = \App\Order::where('is_checked_out', '=', false)//هات اخر اوردر من جدول الاوردرات للمستخدم ده
        ->where('user_id', '=', \Auth::user()->id)//بشرط اليوزر اى دى اللى فى جدول الاورد يساوى الاى دى للمستخدم اللى عامل لوجين
        ->orderBy('created_at', 'DESC')->first();   //ورتبهم تنازلى من الكبير الى الصغير حسب الانشاء بتاعهم وهات اول واحد

        if ($lastOrder) {   //لو اخر اوردر موجود
            //هات تفاصيل الاوردر من جدول الـ OrderDetails بشرط ان الاوردر بتاع الاى دى يكون نفس الاوردر اللى انا جبته
            $products = \App\OrderDetail::where('order_id', '=', $lastOrder->id)->get();
            foreach ($products as $product) {   //لف على كل المنتجات اللى فى الاوردر واحد واحد
                $product->delete();            //وامسحهم من جدول orderDetails
            }
            $lastOrder->delete();  //وبعدين بعد ماتخلص  وتمسح تفاصيل الاوردر امسح الاوردر ذات نفسة  من جدول الorders
            return redirect('/shopping-cart')//حولة على صفحة كارت التسوق
            ->with(['success' => 'Order is cancelled successfully!']); //واعمله رسالة نجاح

        } else {   //لو اخر اوردر مش موجود
            return redirect('/shopping-cart');  //حولة على صفحة كارت التسوق
        }
    }
}





