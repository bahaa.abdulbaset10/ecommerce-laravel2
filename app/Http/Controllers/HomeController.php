<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{


    public function index(){   //هيجبلنا صفحة الاندكس الاولى الصفحة الرئيسية للموقع

        return view('index');
    }


    public function getHomeView(){  //الداله دى بتخدم كل مايخص فى صفحة الهوم من منتجات واقسام وسلايدر وغيره
        //هنا بنقولة هات من جدول الاسليدر 10 اسليدر بس مترتبين حسب اخر 10 وهاتهم وحطهم فى متغير
        $sliderData = \App\Slider::orderBy('created_at','DESC')->take(10)->get();

        //هات اقدم 10 اقسام للمنتجات موجودين فى جدول الاقسام
        $cat =  $cats = \App\Category::orderBy('created_at','ASC')->take(10)->get();


        //هات اجدد 12 منتج موجودين فى جدول المنتجات
        $newproducts = \App\Product::orderBy('created_at', 'DESC')->take(12)->get();



        //هعمل جروب اجيب الاوردرات كلها واشوف المنتج اتباع كام مرة وارتبهم DESC واختار اول 10 id
        $topsales = \App\OrderDetail::groupBy('product_id')   //هنا بنقولة هات من جدول OrderDetails  الاى دى بتاع المنتج
            ->select('product_id', \DB::Raw('COUNT(*) AS k')) //   هنا بنقولة هات الاى دى بتاع المنتج و افتح الدتا بيس واعمل عمود وسميه k علشان يجيب المجموع بتاع الحاجات اللى اتباعت
            ->orderBy('k','DESC') //ورتب الحاجات اللى فى العمود اللى عملنا ترتيب تنازلى من الكبير للصغير
            ->take(12) // هات اول 12 واحد
            ->pluck('product_id') // جبنا العمود بتاع الاى دى للمنتج
            ->toArray(); //حولهم لمصفوفة
        $sales =\App\Product::find($topsales); //هات التفاصيل بتاع الاى دى بتاع المنتج والمجموع المنتجات الى اتباعت من جدول المنتجات وحطهم فى المتغير$sales
        //الغرض منه شوفلى المبيعات رقم المنتج كذا اتباع اد كذا اكتر مبيعات للidبتاع المنتج

        return view('home',[   //هبعتهم كا متغير جنب ال home
            'sliders'=> $sliderData, //علشان هنستخدم sliders فى صفحة الهوم علشان يجيب الصور بتاعت الاسليد
            'cats' => $cat,         //علشان هنستخدم'cats'فى صفحة الهوم بردو علشان نجيب اقدم 10 اقسام للمنتجات
            'arrivals'=> $newproducts,  //علشان هنستخدم 'product' فى صفحة الهوم علشان يجيب احدث 12 منتج
            'sales' => $sales,        //علشان هنستخدم 'product' فى صفحة الهوم علشان يجيب الـ12 منتج الاعلى مبيعا
        ]);
    }








    public function addToCart(Request   $request){  //عملنا داله علشان لما نضغط على الزرار بتاع المنتج يشترية ويحطه فى كارت التسوق
            $productID = $request->get('product'); //هات من الزرار اللى عامله مخفى المنتج وحطه فى متغير

        if(!\Auth::check()){  //لو المستخدم ضغط على الزرار من غير ما يعمل لوجن
          return redirect('/login');  //ارميه بره على صفجة اللوجين
      }
      //القواعد
      $rules=['product' => 'required'];   //اسم المنتج دا مطلوب product دى اللى معمولة فى الزرار المخفى فى الview بتاع الـproduct
        $validator = \Validator::make($request->all(), $rules); //هات كل البيانات والقواعد اللى انا حطتهالك تمشى عليها

      if($validator->fails()){     //هعمل فالديشن لو المنتج مش موجود يرجعه لصفحة الهوم مرة تانيه
          return redirect('/home');
      }
      $productID = $request->get('product'); //ولو ما فشلش خليه يجيب البيانات ويحطها فى المتغير اللى هو$productID

        $product = \App\Product::find($productID); //هات الاى دى بتاع المنتج من جدول المنتجات
        if(!$product){  //لو المنتج مش موجود فى الدتا بيس او مفيش منتج
            return redirect('/home');  //حولة على صفحة الهوم
        }

        $lastOrder= \App\Order::where('is_checked_out', '=',false) //هات اخر اوردر من جدول الاوردرات للمستخدم ده
        ->where('user_id', '=', \Auth::user()->id)   //بشرط اليوزر اى دى اللى فى جدول الاورد يساوى الاى دى للمستخدم اللى عامل لوجين
        ->orderBy('created_at', 'DESC')->first();   //ورتبهم تنازلى من الكبير الى الصغير حسب الانشاء بتاعهم وهات اول واحد

        if($lastOrder) {
           //لو الاوردر موجود او المنتج دا اشتراه قبل كده ولا لاء معنى كده ان هزود عليه
        $orderProduct = \App\OrderDetail::where('order_id', '=', $lastOrder->id )  //هات الاى دى بتاع الاوردر اللى انا لسه بعمله وشوفه بيساوى اخر اوردر من جدول الاوردرات ولا لاء
        ->where('product_id', '=', $product->id)->first();  //و الاى دى بتاع المنتج يساوى نفس المنتج اللى انا لسه جايبه هات اول واحد

           //هل انا اشتريت منتج ولا لا لو انا اشتريته يزوده بواحد لو مشترتوش يشترى من جديد
           if($orderProduct){ //لو لقاه هيجيب الاوردر القديم ويزود عليه بواحد
               $orderProduct->amount = $orderProduct->amount + 1; //هروح على جدول تفاصيل الاوردر فى حقل الكمية وازود عليها واحد
               $orderProduct->save();  //وبعدين احفظ البيانات فى الدتا بيس
           }else{ //ولو ملقهوش اعملى اوردر جديد
               //وبعدين هسجل الاوردر فى جدول OrderDetails
            $newOrderDetails = new \App\OrderDetail();  //سجل المنتج فى جدول تفاصيل الاوردر

            $newOrderDetails->order_id = $lastOrder->id; //الاى دى بتاعك اللى انا لسه جايبه من فوق lastOrder$
            $newOrderDetails->product_id = $productID; //الاى دى بتاع المنتج اللى انا لسه جايبه من فوق productID$
            $newOrderDetails->amount = 1;               //الكمية اللى انا عايزها
            $newOrderDetails->price = $product->selling_price;  //سعر المنتج جبته من جدول المنتجات عن طريق الاى دى بتاع المنتج اللى جبته فوق وعملت فالديشن عليه
            $newOrderDetails->discount = $product->discount; //سعر الشراء جبته من جدول المنتجات عن طريق الاى دى بتاع المنتج اللى جبته فوق وعملت فالديشن عليه
            $newOrderDetails->save();    //ودى الكلام دا للدتا بيس
           }
       }else{  //ولو الاوردر مش موجود من قبل هضيفه انا

           //هسجل الاوردر فى جدول الـOrders
           $newOrder = new \App\Order();   //هات جدول الاورد وحطه فى متغير

           $newOrder->user_id = \Auth::user()->id;  //اليوزر اى دى بتاعك هاتاخده من جدول اليوزر من حق ال id
           $newOrder->first_name = ""; //الاسم الاول فاضى
           $newOrder->last_name = ""; //الاسم التانى فاضى
           $newOrder->address = "";  //العنوان فاضى
           $newOrder->phone_number = ""; //الموبايل فاضى
           $newOrder->save(); //واحفظ


           //وبعدين هسجل الاوردر فى جدول OrderDetails
           $newOrderDetails = new \App\OrderDetail();  //سجل المنتج فى جدول تفاصيل الاوردر

           $newOrderDetails->order_id = $newOrder->id; //الاى دى بتاعك اللى انا لسه جايبه من فوق newOrder$
           $newOrderDetails->product_id = $productID; //الاى دى بتاع المنتج اللى انا لسه جايبه من فوقproductID$
           $newOrderDetails->amount = 1;               //الكمية اللى انا عايزها
           $newOrderDetails->price = $product->selling_price;  //سعر المنتج جبته من جدول المنتجات عن طريق الاى دى بتاع المنتج اللى جبته فوق وعملت فالديشن عليه
           $newOrderDetails->discount = $product->discount; //سعر الشراء جبته من جدول المنتجات عن طريق الاى دى بتاع المنتج اللى جبته فوق وعملت فالديشن عليه
           $newOrderDetails->save();    //ودى الكلام دا للدتا بيس
       }

        return redirect('/home');  //  بعد ما يخلص خالص يحولة على صفحة الهوم
    }

}
