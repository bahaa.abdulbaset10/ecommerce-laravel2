<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;

class AuthAPIController extends Controller
{

    //*******وظيفة الداله دى بتجيب بيانات الاسم والباسورد من الدتات بيس وتعمل اتشك عليهم هل صح ولا غلط **********
    public function doLogin(Request $request){ //    علشان اجيب الدتا من اى فورم موجودة فى صفحة HTML
        $data = $request->all();   //بتجيب كل الدتا اللى اتبعت سواء جيت او سواء بوست
        $rules = [   //عملنا اسوشيتف اراى علشان نحط جواها الحاجات الفالديشن المطلوبة على كل تكست بوكس
            //الا سوشيتف اراى بتاخد key و value
            'username' => 'required|min:5|max:100',   //بنقولة الكاى بتاعك اليوزر نيم وعايزينه نوع التكست مهم واقل كتابه فيه 5 حروف واكتر كتابة 100 حرف
            'password' => 'required|min:5|max:125',    //بنقولة الكاى بتاعك الباسورد وعايزينه نوع التكست مهم واقل كتابه فيه 5 حروف واكتر كتابة 125 حرف
        ];
        // بنعمل متغير اسمه فلاديتور بياخد خصائص الفلاديتور ويعدين بنديله البيانات اللى عايزينه يعمل عليها وبنديله القواعد
        $validator = \Validator::make($data, $rules) ;
        if($validator -> fails()){    //بنقولة لو الفالديشن فشل
            $reply = [                                         //عملنا اسوشيتف ارراى
               'failed' => true,                               // قولناله لو فى ايرور
               'errors' => $validator->errors()->toArray(),       // طلعله الايرور
               'data' => null,                                  // لو الدتا فاضية
           ];
           return response()->json($reply);                   // ارجع بالبيانات فى هيئة جاسون وهيطلع الايرور
        }
        //فى موديول جاهز اسمه Auth فى الارافيل بيعمل اتشك لل login هل اللوجين داخل ولا  لاء وجواه داله اسمها  attempt ببعتلها اسوشيتف اررى اجط فيها البينات اللى هتشك عليها
        if (\Auth::attempt(['username' => $data['username'], 'password' => $data['password']])) {
            $apiToken = sha1(time()); //شفر الوقت وحطه فى متغير
            $user = \Auth::user(); //هنجيب اليوزر اللى عمل لوجين
            $user->api_token = $apiToken; //وهنقولة اليوزر اللى عمل api_token هو المتغير بتاع تشفير الوقت اللى عملنها
            $user->save(); //واعمل حفظ

            $reply = [
                'failed' => false,    //عملنا اسوشيتف ارراى
                'errors' => null,     // قولناله لو مفيش ايرور
                'data' => $user,      // هات بيانات اليوزر اللى عمل لوجن
            ];
            return response()->json($reply);      //وحط بيانات اليوزر اللى عمل لوجين فى ملف على هيئة جاسون
        } else {
            $reply = [                                         //عملنا اسوشيتف ارراى
                'failed' => true,                               // قولناله لو فى ايرور
                //واعمل رسالة ايرور فى اسوشيتف اراى تقولة فى خطأ فى الاسم او الباسورد
                'errors' => ['login' =>'username or/and password is/are wrong!!'],  // طلعله الايرور
                'data' => null,                                  // لو الدتا فاضية
            ];
            return response()->json($reply);                   // ارجع بالبيانات فى هيئة جاسون وهيطلع الايرور
        }
    }
}






























