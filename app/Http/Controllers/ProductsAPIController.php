<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductsAPIController extends Controller
{

    //*******دى الفانيكشن بتاعت المنتج اللى انا اخترته فقط *****
   public function getProductDetails(Request $request, $pID){
       //هات الاى دى بتاع المنتج من جدول المنتجات بشرط الاى دى اللى اختارته هو الاى دى اللى فى متغير المودل وهتلى الصورة كمان وحطها مع المنتج
       $product = \App\Product::where('id', '=', $pID)->with('image')->first();
                        //هات المنتج مع ال image اللى هيا الصورة اللى هى الفانيكشن الموجودة فى المودل بتاع Product واعرضها

       if ($product) {  //لو المنتج موجود
           $reply = [                                 //عملنا اسوشيتف ارراى لو تمت العملية بنجاح
               'failed' => false,                      //قوله مفيش فشل ولا ايرور
               'errors' => null,                      //ومتعرضش الايرور خليه فاضى
               //الproducts دى الفانيكشن بتاعت العلاقة الربط الموجودة فى المودل بتاعت Product
               'data' => $product, // واطبع المنتج اللى انا اختارته
           ];
           return response()->json($reply);   // ارجع بالبيانات فى هيئة جاسون

       } else {   //لو المنتج مش موجود
           $reply = [                                         //عملنا اسوشيتف ارراى
               'failed' => true,                               // قولناله لو فى ايرور
               'errors' => ['Product is not found.'],         // طلعله الايرور
               'data' => null,                                 //وخلى الدتا فاضية
           ];
           return response()->json($reply);     // ارجع بالبيانات فى هيئة جاسون وهيطلع الايرور
       }
   }
}


//استخدمنا first(); فوق علشان لان ()get بترجعلى array و array مش null
//لكن ال first(); بيطبع عنصر ف العنصر ممكن يكون null عادى جدا








