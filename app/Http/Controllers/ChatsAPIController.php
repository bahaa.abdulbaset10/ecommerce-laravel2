<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ChatsAPIController extends Controller
{

    //*******وظيفة الداله دى بتجيب الشاتات الموجودة فى الدتا بيس الجزء اللى على الشمال اللى فيه
    // الاسم واخر رسالة والتاريخ والصورة الشاتات اللى تحت بعضها فى الشمال  **********
    public function getChats(Request $request)  //    علشان اجيب الدتا من اى فورم موجودة فى صفحة HTML
    {$apiToken = $request->get('api_token');   //بتجيب كل الدتا اللى اتبعت سواء جيت او سواء بوست
        $rules = [   //عملنا اسوشيتف اراى علشان نحط جواها الحاجات الفالديشن المطلوبة على كل تكست بوكس
            //الا سوشيتف اراى بتاخد key و value
            'api_token' => 'required|max:125',   //*****بنعمل اتشك هل الapi سليم ولا لاء *****//       الapiمطلوب ونجيب منه اقصى حاجه 125 حرف
        ];
        // بنعمل متغير اسمه فلاديتور بياخد خصائص الفلاديتور ويعدين بنديله البيانات اللى عايزينه يعمل عليها وبنديله القواعد
        $validator = \Validator::make($request->all(), $rules);
        if($validator -> fails()){    //لو الapi فى مشكلة
            $reply = [                                         //عملنا اسوشيتف ارراى
                'failed' => true,                               // قولناله لو فى ايرور
                'errors' => $validator->errors()->toArray(),       // طلعله الايرور
                'data' => null,                                  // لو الدتا فاضية
            ];
            return response()->json($reply);                   // ارجع بالبيانات فى هيئة جاسون وهيطلع الايرور
        }  //طب لو لاء هنشتك هل موجود فى الدتا بيس ولا لاء
        //روح لجدول اليوزر وقوله هات الapi_token بشرط يساوى ال api_token اللى جاتلك واعرض اول مستخدم
        $user = \App\User::where('api_token','=' ,$apiToken)->first();
        //طب لو لقى ال api_token
        if($user){ //هات الرسايل بتاعته ال api_token ده
            $data = [];   //Chats
            $sentMessages = $user->sentMessages;     //الرسايل اللى مبعوتة جبتها من جدول اليوزر
            $sendUserIDs = $sentMessages->pluck('second_user_id')->unique()->toArray(); //هات الاى دى بتاع الشخص اللى باعتلى الرسالة وحط فى المتغير وقولناله  متكررش
            $sendUsers = \App\User::find($sendUserIDs);  //الناس اللى بعتتلى رسايل
            foreach ($sendUsers as $sendUser){
                $lastMessage = \App\Chat::where('first_user_id', '=', $user->id)//اخر رسالة بالنسبه ليا تساوى الاى دى بتاع اليوزر اللى عمل api_token
                ->where('second_user_id', '=', $sendUser->id)   //بشرط تساوى اخر رسالة للناس اللى بعتتلى
                ->orderBy('created_at','DESC')->first(); //وهرتبهم حسب الرسالة الاخيرة واقوله هات اول رساله تقابلنى
                array_push($data, [  //ضيف داخل المصفوفة ال id و الكلام اللى جوه دا
                    "uID" => $sendUser->id,   //هات الاى دى وحطهم فى المصفوفة
                    "fullName" => $sendUser->first_name . " " . $sendUser->last_name,  //هات الاسم الاول والاسم التان وادمجهم جنب بعض وحط بينهم مسافه
                    "imageUrl" => $sendUser->image->path, //1.جبنا مسار الصورة 2.من فانيكشن الموجوده فى المودل بتاعت اليوزر 3.فى جدول اليوزر
                    "lastMsgContent" => $lastMessage->message,       //محتوى اخر رسالة
                    "lastMsgTsSeen" => $lastMessage->is_seen,         //هل شوفت الرسالة ولا  لا
                    "lastMsgIsForward" => $lastMessage->is_forward,
                    "lastMsgTimestamp" => $lastMessage->created_at,  //هات تاريخ ووفت انشاء الرسالة
                ]);
            }

            $recMessages = $user->receivedtMessages;  // الرسايل المستلمه  جبتها من جدول اليوزر
            $recUserIDs = $recMessages->pluck('first_user_id')->unique()->toArray(); //هات الاى دى بتاع الشخص اللى انا باعتله الرسالة وحط فى المتغير وقولناه متكررش
            $recUsers = \App\User::find($recUserIDs);   //الناس اللى انا بعتلها رسايل
            foreach ($recUsers as $recUser){
                $lastMessage = \App\Chat::where('second_user_id', '=', $user->id)  //اخر رسالة بالنسبه للناس اللى بعتتلى تساوى الاى دى بتاع اليوزر اللى عمل api_token
                ->where('first_user_id', '=', $recUser->id)    //تساوى اخر رسالة بالنسبه ليا
                ->orderBy('created_at','DESC')->first(); //وهرتبهم حسب الرسالة الاخيرة واقوله هات اول رساله تقابلنى
                array_push($data, [  //ضيف داخل المصفوفة ال id و الكلام اللى جوه دا
                    "uID" => $recUser->id,   //هات الاى دى وحطهم فى المصفوفة
                    "fullName" => $recUser->first_name . " " . $user->last_name,  //هات الاسم الاول والاسم التان وادمجهم جنب بعض وحط بينهم مسافه
                    "imageUrl" => $recUser->image->path, //1.جبنا مسار الصورة 2.من فانيكشن الموجوده فى المودل بتاعت اليوزر 3.فى جدول اليوزر
                    "lastMsgContent" => $lastMessage->message,       //محتوى اخر رسالة
                    "lastMsgTsSeen" => $lastMessage->is_seen,         //هل شوفت الرسالة ولا  لا
                    "lastMsgIsForward" => $lastMessage->is_forward,
                    "lastMsgTimestamp" => $lastMessage->created_at,  //هات تاريخ ووفت انشاء الرسالة
                ]);
            }
            $reply = [                        //لو تمت العملية بنجاح
                'failed' => false,           //قوله مفيش فشل ولا ايرور
                'errors' => null,            //ومتعرضش الايرور
                'data' => $data,               //اطبع الشاتات
            ];
            return response()->json($reply);  //اعرضلة الكلام دا فى هيئة جاسون
        }else{  //طب لو ملقهوش معنى كده ان ال api_token  غلط او مش موجود
            $reply = [                                         //عملنا اسوشيتف ارراى
                'failed' => true,                               // قولناله لو فى ايرور
                'errors' => ['wrong api token.'],                 // طلعله الايرور
                'data' => null,                                  // لو الدتا فاضية
            ];
            return response()->json($reply);                   // ارجع بالبيانات فى هيئة جاسون وهيطلع الايرور
        }
    }



    //*******وظيفة الداله دى بتجيب الشاته اللى انا اخترتها من الشمال وضغط عليها
// بتعرضلى الشات كامل اللى بينى وبين الى بيراسلنى على اليمين **********
    public function getMessages(Request $request, $uID){
        //    علشان اجيب الدتا من اى فورم موجودة فى صفحة HTML ولكن هيجبلى المتغير اللى اسمه uID  اللى هو اليوزر اللى انا عايز اكلمه


        $apiToken = $request->get('api_token');   //بتجيب كل الدتا اللى اتبعت سواء جيت او سواء بوست
        $rules = [   //عملنا اسوشيتف اراى علشان نحط جواها الحاجات الفالديشن المطلوبة على كل تكست بوكس
            //الا سوشيتف اراى بتاخد key و value
            'api_token' => 'required|max:125',   //*****بنعمل اتشك هل الapi سليم ولا لاء *****//       الapiمطلوب ونجيب منه اقصى حاجه 125 حرف
        ];

        // بنعمل متغير اسمه فلاديتور بياخد خصائص الفلاديتور ويعدين بنديله البيانات اللى عايزينه يعمل عليها وبنديله القواعد
        $validator = \Validator::make($request->all(), $rules);
        if($validator -> fails()){    //لو الapi فى مشكلة
            $reply = [                                         //عملنا اسوشيتف ارراى
                'failed' => true,                               // قولناله لو فى ايرور
                'errors' => $validator->errors()->toArray(),       // طلعله الايرور
                'data' => null,                                  // لو الدتا فاضية
            ];
            return response()->json($reply);   // ارجع بالبيانات فى هيئة جاسون وهيطلع الايرور
        }  //طب لو لاء هنشتك هل موجود فى الدتا بيس ولا لاء
        //روح لجدول اليوزر وقوله هات الapi_token بشرط يساوى ال api_token اللى جاتلك واعرض اول مستخدم
        $user = \App\User::where('api_token','=' ,$apiToken)->first();

        //طب لو لقى ال api_token
        if($user) { //هات الرسايل بتاعته ال api_token ده



            //عملنا use هنا وحطينا المتغيرين دول علشان المتغيرين بداخل الفانيكشن مبيشوفوش المتغيرات اللى برا علشان كده عرفناهم
            $messages  = \App\Chat::where(function ($query) use ($user, $uID) {
                $query->where('first_user_id', '=', $user->id) //اخر رسالة بالنسبه ليا تساوى الاى دى بتاع اليوزر اللى عمل api_token
                ->where('second_user_id', '=', $uID);   //بشرط تساوى اخر رسالة للناس اللى بعتتلى

            })->orwhere(function ($query) use ($user, $uID) {
                $query->where('second_user_id', '=', $user->id) //اخر رسالة بالنسبه ليا تساوى الاى دى بتاع اليوزر اللى عمل api_token
                ->where('first_user_id', '=', $uID);   //بشرط تساوى اخر رسالة للناس اللى بعتتلى

            })
                ->orderBy('created_at','DESC')->get(); //وهرتبهم حسب اخر رسايل واقوله هات كل الرسايل

            $reply = [                        //لو تمت العملية بنجاح
                'failed' => false,           //قوله مفيش فشل ولا ايرور
                'errors' => null,            //ومتعرضش الايرور
                'data' => $messages,           //اطبع الرسايل
            ];
            return response()->json($reply);  //اعرضلة الكلام دا فى هيئة جاسون

        }else{  //طب لو ملقهوش معنى كده ان ال api_token  غلط او مش موجود
            $reply = [                                         //عملنا اسوشيتف ارراى
                'failed' => true,                               // قولناله لو فى ايرور
                'errors' => ['wrong api token.'],                 // طلعله الايرور
                'data' => null,                                  // لو الدتا فاضية
            ];
            return response()->json($reply);                   // ارجع بالبيانات فى هيئة جاسون وهيطلع الايرور
        }

        }






    //*******وظيفة الداله دى بتبعت رسالة  للطرف التانى  **********
    public function sendMessages(Request $request, $uID){
        //    علشان اجيب الدتا من اى فورم موجودة فى صفحة HTML ولكن هيجبلى المتغير اللى اسمه uID  اللى هو اليوزر اللى انا عايز اكلمه


        $apiToken = $request->get('api_token');   //بتجيب كل الدتا اللى اتبعت سواء جيت او سواء بوست
        $rules = [   //عملنا اسوشيتف اراى علشان نحط جواها الحاجات الفالديشن المطلوبة على كل تكست بوكس
            //الا سوشيتف اراى بتاخد key و value
            'api_token' => 'required|max:125',   //*****بنعمل اتشك هل الapi سليم ولا لاء *****//       الapiمطلوب ونجيب منه اقصى حاجه 125 حرف
            'message' => 'required|max:1000|min:1'  //حقل الرسالة مهم اقصى حروف فى الرسالة 1000 حرف واقل حاجه حرف واحد

        ];

        // بنعمل متغير اسمه فلاديتور بياخد خصائص الفلاديتور ويعدين بنديله البيانات اللى عايزينه يعمل عليها وبنديله القواعد
        $validator = \Validator::make($request->all(), $rules);
        if($validator -> fails()){    //لو الapi فى مشكلة
            $reply = [                                         //عملنا اسوشيتف ارراى
                'failed' => true,                               // قولناله لو فى ايرور
                'errors' => $validator->errors()->toArray(),       // طلعله الايرور
                'data' => null,                                  // لو الدتا فاضية
            ];
            return response()->json($reply);   // ارجع بالبيانات فى هيئة جاسون وهيطلع الايرور
        }  //طب لو لاء هنشتك هل موجود فى الدتا بيس ولا لاء
        //روح لجدول اليوزر وقوله هات الapi_token بشرط يساوى ال api_token اللى جاتلك واعرض اول مستخدم
        $user = \App\User::where('api_token','=' ,$apiToken)->first();

        //طب لو لقى ال api_token
        if($user) { //اعمل رسالة جدية


            $newMessage = new \App\Chat();          //جدول الشات نسخة من متغير newmessage
            $newMessage->first_user_id = $user->id;   //ال first_user_id لى فى جدول الشات يساوى ال id اللى فى جدول اليوزر=>ودا اللى هو انا الراسل الاول
            $newMessage->second_user_id = $uID;       //ال second_user_id الفى جدول الشات يساوى ال $uID اللى هو المتغير بتاع اللى بيبعتلى =>ودا اللى هو الشخص اللى بيبعتلى
            $newMessage->is_seen = false;             //الرسالة هتكون فولس لان اانا اللى باعتها فمش هتفرق معايا المستقبل هو اللى هيبص على الحقل دا
            $newMessage->is_forward = true;           //انا اللى باعت
            $newMessage->message = $request->get('message'); //حط فى حقل المسدج محتوى الرسالة
            $newMessage->save();                     //اعمل حفظ

            $reply = [                        //لو تمت العملية بنجاح
                'failed' => false,           //قوله مفيش فشل ولا ايرور
                'errors' => null,            //ومتعرضش الايرور
                'data' => null,               //ومفيش داتا هبعتها
            ];
            return response()->json($reply);  //اعرضلة الكلام دا فى هيئة جاسون


        }else{  //طب لو ملقهوش معنى كده ان ال api_token  غلط او مش موجود
            $reply = [                                         //عملنا اسوشيتف ارراى
                'failed' => true,                               // قولناله لو فى ايرور
                'errors' => ['wrong api token.'],                 // طلعله الايرور
                'data' => null,                                  // لو الدتا فاضية
            ];
            return response()->json($reply);                   // ارجع بالبيانات فى هيئة جاسون وهيطلع الايرور
        }

    }





}












