<?php

namespace App\Http\Controllers;

use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Authcontroller extends Controller
{

    public function getLoginView(){  //**********دى الداله اللى هننده عليها وظيفتها هتودينا على صفحة  login****************
        if(\Auth::check()){             //فى موديول جاهز اسمه Auth فى الارافيل جواه داله اسمها check بتتشك هو عامل لوجين ولا  لاء
            return redirect('/');
            //هنا عملنا اتشك لو المستخدم عامل لوجين وديه على صفحة الhome
        };
            return view('auth.login');
            //ولو مش عامل لوجين وديه على صفحة ال login
    }




                                                                        //*******وظيفة الداله دى بتجيب بيانات الاسم والباسورد من الدتات بيس وتعمل اتشك عليهم هل صح ولا غلط **********
    public function doLogin(Request $request){ //    علشان اجيب الدتا من اى فورم موجودة فى صفحة HTML
        $data = $request->all();   //بتجيب كل الدتا اللى اتبعت سواء جيت او سواء بوست


       $rules = [   //عملنا اسوشيتف اراى علشان نحط جواها الحاجات الفالديشن المطلوبة على كل تكست بوكس
           //الا سوشيتف اراى بتاخد key و value
           'username' => 'required|min:5|max:100',   //بنقولة الكاى بتاعك اليوزر نيم وعايزينه نوع التكست مهم واقل كتابه فيه 5 حروف واكتر كتابة 100 حرف
           'password' => 'required|min:5|max:125',    //بنقولة الكاى بتاعك الباسورد وعايزينه نوع التكست مهم واقل كتابه فيه 5 حروف واكتر كتابة 125 حرف
       ];
       // بنعمل متغير اسمه فلاديتور بياخد خصائص الفلاديتور ويعدين بنديله البيانات اللى عايزينه يعمل عليها وبنديله القواعد
       $validator = \Validator::make($data, $rules) ;
       if($validator -> fails()){    //بنقولة لو الفالديشن فشل
           return redirect('/login')          //حولة على صفحة تسجيل الدخول
           ->withInput($request->all())  // وبعدين الحاجه القديمة اللى كنت كاتبها فى التكست بوكس سيبها زى ما هيا متعملهاش ريفريش
           ->WithErrors($validator->errors()); // وبعدين طلعله الايرور
       }
                //فى موديول جاهز اسمه Auth فى الارافيل بيعمل اتشك لل login هل اللوجين داخل ولا  لاء وجواه داله اسمها  attempt ببعتلها اسوشيتف اررى اجط فيها البينات اللى هتشك عليها
        if (\Auth::attempt(['username' => $data['username'], 'password' => $data['password']])) {
            return redirect('/');      //قولناله لو الاسم والباسورد صح بعد ماتعمل لوجين حوله الى صفحة ال home
        } else {
            return redirect('/login')  //ولو مش عامل لوجين حوله على صفحة ال login
             ->withInput($request->all())  // وبعدين الحاجه القديمة اللى كنت كاتبها فى التكست بوكس سيبها زى ما هيا متعملهاش ريفريش
            ->withErrors(['login' =>'username or/and password is/are wrong!!']); //واعمل رسالة ايرور فى اسوشيتف اراى تقولة فى خطأ فى الاسم او الباسورد
        }
    }





                                                                                     //**********دى الداله اللى هننده عليها وظيفتها هتودينا على صفحة  register****************
    public function getRegisterView(){
                                  //فى موديول جاهز اسمه Auth فى الارافيل جواه داله اسمها check بتتشك هو عامل لوجين ولا  لاء
        if(\Auth::check()){
            return redirect('/');
            //هنا عملنا اتشك لو المستخدم عامل لوجين متكملش
        };
        return view('auth.register');
        //ولو مش عامل لوجين وديه على صفحة ال register
    }



    public function doRegistration(Request $request)      //*******وظيفة الداله دى بتعمل فالديشن على الحقول اللى عندى فى فورم الريجستريشن من الدتات بيس وتعمل اتشك عليهم هل صح ولا غلط **********
    { //    علشان اجيب الدتا من اى فورم موجودة فى صفحة HTML
        $data = $request->all();   //بتجيب كل الدتا اللى اتبعت سواء جيت او سواء بوست

        $rules = [   //عملنا اسوشيتف اراى علشان نحط جواها الحاجات الفالديشن المطلوبة على كل تكست بوكس
            //الا سوشيتف اراى بتاخد key و value
            'firstName' => 'required|min:5|max:60',   //بنقولة الكاى بتاعك اليوزر نيم وعايزينه نوع التكست مهم واقل كتابه فيه 5 حروف واكتر كتابة 100 حرف
            'lastName' => 'max:60',   //اكتر حاجه تكتب فى التكست بوكس  60
            'username' => 'required|min:5|max:100|unique:users,username',   // بنقولة حقال اليوزر نيم مهم واقل حاجه تكتب فيه تكون 5 واكتر حاجه تكتب فيه تكون 100ويكون مبيتكررش
            'email' => 'required|min:5|max:125|email|unique:users,email',    //بنقولة حقل الايميل مهم اقل من 5 فى التكست بوكس واكتر حاجه بتكون 125 ويكون نوعه ايميل ومبيتكررش
            'password' => 'required|min:5|max:100',   //بنقولة حقل الباسورد مهم ولازم يكون اقل من 5 حروف واكتر حاجه مكتوبة فيه 100 حرف
            'gender' => '',    //النوع هنا معملناش فيه حاجه علشان هنستدعيه من الدتا بيس
            'phoneNumber' => 'max:100',   // اكتر حاجه 100
            'address' => 'max:125',    //العنوان اكتر حاجه 125 حرف
            'bio' => 'max:1000',   //تكست التعليقات فيه 1000 حرف

        ];

        // بنعمل متغير اسمه فلاديتور بياخد خصائص الفلاديتور ويعدين بنديله البيانات اللى عايزينه يعمل عليها وبنديله القواعد
        $validator = \Validator::make($data, $rules);
        if ($validator->fails()) {           //بنقولة لو الفالديشن فشل
            return redirect('/register')//حولة على صفحة الريجستريشن مرة تانيه
            ->withInput($request->all())  // وبعدين الحاجه القديمة اللى كنت كاتبها فى التكست بوكس سيبها زى ما هيا متعملهاش ريفريش
            ->WithErrors($validator->errors()); // وبعدين طلعله الايرور
        }


                        //اسم المودل
        $newUser = new \App\User();

        $newUser->first_name = $data['firstName'];
        $newUser->last_name = $data['lastName'];
        $newUser->email = $data['email'];
        $newUser->username = $data['username'];
        $newUser->password = bcrypt($data['password']);
        $newUser->gender_id = $data['gender'];
        $newUser->bio = $data['bio'];
        $newUser->phone_number = $data['phoneNumber'];
        $newUser->address = $data['address'];

        $newUser->save();
        //بكده حط البيانات بتاعته جوه الدتا بيس او احفظ الريكورد بتاعك

        return redirect('/login') //بعد ما تحفظ حولنى على صفحة اللوجين او تسجيل الدخول

        ->with(['success'=>'you are registerd successfully']);  //وبعدين اعرضله الرسالة دى

    }












    public function doLogout(){                                             //*******وظيفة الداله دى اما المستخدم يعمل logout تحولة على صفحة الlogin من جديد**********
        \Auth::logout();
        return redirect('/login');

    }

}
