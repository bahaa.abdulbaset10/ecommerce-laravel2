<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoriesAPIController extends Controller
{     //*******دى الفانيكشن بتاعت الاقسام التى تحتوى على  المنتجات  *****
    public function index(Request $request)
    {
        //هات المودل بتاع الكاتيجورى وهات معاه الصور اللى هوimage الفانيكشن الموجودة بداخل المودل بتاع الكاتيجورى وحطه جوه متغير
        $categories = \App\Category::with('image')->get();
        $reply = [                             //عملنا اسوشيتف ارراى لو تمت العملية بنجاح
            'failed' => false,                //قوله مفيش فشل ولا ايرور
            'errors' => null,                  //ومتعرضش الايرور خليه فاضى
            'data' => $categories,              //اعرض الأقسام بتاعت المنتجات
        ];
        return response()->json($reply);    //اعرضلة الكلام دا فى هيئة جاسون
    }


    //*******دى الفانيكشن بتاعت كل المنتجات الموجودة بداخل الاقسام   *****
    public function getCatProducts(Request $request, $cID)   //هيستقبل الاى دى بتاع المنتج
    {  $category = \App\Category::find($cID); //هات الاى دى بتاع المنتج من جدول الكاتيجروى

        if ($category) {  //لو الكاتيجورى موجود
            $reply = [                                 //عملنا اسوشيتف ارراى لو تمت العملية بنجاح
                'failed' => false,                      //قوله مفيش فشل ولا ايرور
                'errors' => null,                      //ومتعرضش الايرور خليه فاضى
                                      //الproducts دى الفانيكشن بتاعت العلاقة الربط الموجودة فى المودل بتاعت category
                'data' => $category->products()->with('image')->get(), // واطبع المنتجات اللى جوه الكاتيجورى
                //هات المنتجات مع ال image اللى هيا الصورة اللى هى الفانيكشن الموجودة فى المودل بتاع Category واعرضها
            ];
            return response()->json($reply);    // ارجع بالبيانات فى هيئة جاسون

        } else {   //لو الكاتيجورى مش موجود
            $reply = [                                         //عملنا اسوشيتف ارراى
                'failed' => true,                               // قولناله لو فى ايرور
                'errors' => ['products are not found in the Category.'],   // طلعله الايرور
                'data' => null,                                 //وخلى الدتا فاضية
            ];
            return response()->json($reply);     // ارجع بالبيانات فى هيئة جاسون وهيطلع الايرور
        }
    }
}
