<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function getContactUsView() {  //دى الداله اللى هننده عليها وظيفتها هتودينا على صفحة كونتكت اس
           return view('contact-us');
    }


    //وظيفة الداله دى تحفظ البينات اللى كتبناها فى الكونتكت اس وترميها فى الدتا بيس
    public function saveFeedback(Request $request) {    //    علشان اجيب الدتا من اى فورم موجودة فى صفحة HTML
        $data = $request->all();  //بتجيب كل الدتا اللى اتبعت سواء جيت او سواء بوست

        $rules = [       //عملنا القواعد بتاعت الفالديشن
            'name' => 'required|max:60|min:3',   //الاسم مهم واكتر حروف ليه 60 واقل حروف ليه 3
            'phone' => 'required|max:25',        //الفون مهم واكتر حروف ليه 25
            'feedback' => 'required|max:1000',  //حقل التعليقات مهم اكتر حروف فيه 1000 حرف
            'email' => 'required|email|max:125', //الايميل مهم ويكون نوعه ايميل واكتر حروف ليه 125 حرف
        ];

        $validator = \Validator::make($data, $rules);    // بنعمل متغير اسمه فلاديتور بياخد خصائص الفلاديتور ويعدين بنديله البيانات اللى عايزينه يعمل عليها وبنديله القواعد
        if($validator->fails()){           //بنقولة لو الفالديشن فشل
            return redirect('/contact-us')   //حولة على صفحة contact_us
            ->withInput($data)  // وبعدين الحاجه القديمة اللى كنت كاتبها فى التكست بوكس سيبها زى ما هيا متعملهاش ريفريش
            ->withInput($request->all())  // وبعدين الحاجه القديمة اللى كنت كاتبها فى التكست بوكس سيبها زى ما هيا متعملهاش ريفريش
            ->withErrors($validator->errors());   // وبعدين طلعله الايرور

        }
        //اسم المودل
        $element =new \App\Feedback();

        $element->name =$data['name'];
        $element->phone =$data['phone'];
        $element->email =$data['email'];
        $element->feedback =$data['feedback'];
        $element->save();  //بكده حط البيانات بتاعته جوه الدتا بيس او احفظ الريكورد بتاعك
        return redirect('/contact-us')    //بعد ما تحفظ حولنى على نفس الصفحة بتاعت كونتكت اس تانى
        ->with(['success'=>'Feedback is sent successfully!']);      // واعمل رسالة نجاح success
    }
}








