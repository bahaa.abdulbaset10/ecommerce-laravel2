<?php

namespace App\Http\Middleware;

use Closure;

class testMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //الكونديشن بيجيب ياترو يافولس
        $cond = true;
// هنا بنقولة لو الكونديشن بترو كمل
        if ($cond){
              //هنا بنقوله هات اللى بعده
            return $next($request);

        }else   // لو الكونديشن مكنش بترو
        {
            return redirect('/');  // حوله على الصفحة دى
        }
    }
}
