<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});




//*******(صفحة Api)*********
// 1.الداله اللى جوه الكنترولر   2.اسم الكنترولر اللى انشأنا   3. مسار الصفحة اللى عايزينه يروحها

Route::post('/login','AuthAPIController@doLogin');






//*******(صفحة chatting)*********
//    كلمه لوجين <=   المشترك بينهم وبنقولة البداية بتاعتك كلمة شاتنج
Route::group(['prefix' => 'chatting'], function (){

    // 1.الداله اللى جوه الكنترولر   2.اسم الكنترولر اللى انشأنا   3. مسار الصفحة اللى عايزينه يروحها
 Route::get('/', 'ChatsAPIController@getChats');   //فى حالة اما اجيب اخر رسالة ودى الرسايل بتاعت الجزء اللى فى الشمال
    Route::get('/{uID}/messages', 'ChatsAPIController@getMessages'); //فى حالة اما اجيب كل الرسايل لمستخدم معين الجزء اللى فى اليمين
    Route::post('/{uID}/send', 'ChatsAPIController@sendMessages');    //فى حالة ارسال رسالة جزء بتاع ارسال رسالة

  //حطينا uIDجوا كيرل بريسس علشان دا رقم الاى دى بتاع الرسالة اللى انا هضغط عليها فتحولنى لصفحه /messages هيبقى بالمنظر دا =>
  //localhost/api/chatting/90/messages?api_token=c6501f80525f321ecbbb7745a9f5f1d863a80d99
});









//*******(صفحة categories)*********

    Route::group(['prefix' => 'categories'],function (){  //بدايتك categories
        Route::get('/', 'CategoriesAPIController@index');   //الاقسام التى تشمل المنتجات
           //localhost/api/categories/
        Route::get('/{cID}/products', 'CategoriesAPIController@getCatProducts');    //المنتجات
        //حطينا uIDجوا كيرل بريسس علشان دا رقم الاى دى بتاع المنتج اللى انا هضغط عليها فتحولنى لصفحه المنتج بالشكل دا =>
         //localhost/api/categories/1/products
    });




//*******(صفحة products)*********

Route::group(['prefix' => 'products'],function (){  //بدايتك products
    Route::get('/{pID}/details', 'productsAPIController@getProductDetails');    //منتج واحد فقط
    //حطينا uIDجوا كيرل بريسس علشان دا رقم الاى دى بتاع المنتج اللى انا هضغط عليها فتحولنى لصفحه المنتج بالشكل دا =>
    //localhost/api/products/2/details
});















