<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//*******(صفحة login)*********
// 1.الداله اللى جوه الكنترولر   2.اسم الكنترولر اللى انشأنا   3. مسار الصفحة اللى عايزينه يروحها

//    كلمه لوجين <=   المشترك بينهم
Route::group(['prefix' => 'login'], function (){

    Route::get('/', 'Authcontroller@getLoginView')->name('login');  //عملنا نيمينج لصفحة اللوجين
    //ودناه على الصفحة اللى عايزينه يروحها على طول
    Route::post('/', 'Authcontroller@doLogin');
    //الداله دى بتجيب بيانات الاسم والباسورد من الدتات بيس وتعمل اتشك عليهم هل صح ولا غلط
});




//*******(صفحة register)*********
// 1.الداله اللى جوه الكنترولر   2.اسم الكنترولر اللى انشأنا   3. مسار الصفحة اللى عايزينه يروحها

//    كلمه لوجين <=   المشترك بينهم
Route::group(['prefix' => 'register'], function (){
    Route::get('/', 'Authcontroller@getRegisterView');  //عملنا نيمينج لصفحة اللوجين
    //ودناه على الصفحة اللى عايزينه يروحها على طول
    Route::post('/', 'Authcontroller@doRegistration');
    //الداله دى بتجيب بيانات الاسم والباسورد من الدتات بيس وتعمل اتشك عليهم هل صح ولا غلط
});






//*******(صفحة logout)*********
// 1.الداله اللى جوه الكنترولر   2.اسم الكنترولر اللى انشأنا   3. مسار الصفحة اللى عايزينه يروحها
Route::get('/logout', 'Authcontroller@doLogout');   //ودناه على الصفحة اللى عايزينه يروحها بعد مايعمل تسجيل خروج او logout



//*******(صفحة contact-us)*********ا
//لو انا عايز اطبق على  المديل وير على صفحة الكونتكت اس
//    كلمه كونتكت اس <=   المشترك بينهم
//هنا بقولة الميدل وير عبارة عن تست وحطنها جوا قوسين علشان تكون مصفوفة بحيث ممكن احط اكتر من ميدل وير جوا وليكن الauth ودى ميدل وير جاهزة
//هنا الميدل وير هتطبق فقط على صفحة الكونتكت اس
route::group(['prefix' => 'contact-us','middleware'=>[]], function (){
    Route::get('/', 'ContactController@getContactUsView');  //ودناه على الصفحة اللى عايزينه يروحها
    Route::post('/', 'ContactController@saveFeedback');      //حطينا البيانات جوا الدتا بيس
});




//*******(صفحة home)*********ا
//لو انا عايز اطبق على  المديل وير على صفحة الهوم
//    كلمه الاندكس اللى هى تعنى الlocalhost فقط ولايوجد وراه اى سلاش ولا اى حاجه <=   المشترك بينهم
//هنا بقولة الميدل وير عبارة عن middleware وحطنها جوا قوسين علشان تكون مصفوفة بحيث ممكن احط اكتر من ميدل وير جوا وليكن الauth ودى ميدل وير جاهزة
route::group(['prefix' => '/','middleware'=>[]], function (){
    Route::get('/', 'HomeController@index');  //ودناه على الصفحة اللى index
    Route::get('/home', 'HomeController@getHomeView');  //ودناه على الصفحة اللى home
    Route::post('/home', 'HomeController@addToCart');  //ودناه على الصفحة اللى home

});





//*******(صفحة shopping-cart)*********ا
//لو انا عايز اطبق على  المديل وير على صفحة shopping-cart
//    كلمه الاندكس اللى هى تعنى الlocalhost فقط ولايوجد وراه اى سلاش ولا اى حاجه <=   المشترك بينهم
//هنا بقولة الميدل وير عبارة عن middleware وحطنها جوا قوسين علشان تكون مصفوفة بحيث ممكن احط اكتر من ميدل وير جوا وليكن الauth ودى ميدل وير جاهزة
route::group(['prefix' => '/shopping-cart','middleware'=>['auth']], function (){  //ولازم المستخدم يكون عامل auth لوجين
    Route::get('/', 'CartController@index');    //ودناه على داله الاندكس  indexبتاعت كارت الشراء
    Route::post('/delete', 'CartController@deleteItemFromCart');  //ودناه على داله المسح الى بتمسح عنصر من الاوردر
    Route::post('/delete-all', 'CartController@cancelOrder');  //ودناه على داله اللى بتمسح الاوردر كله

});














//*******(صفحة upload file)*********
// 1.الداله اللى جوه الكنترولر   2.اسم الكنترولر اللى انشأنا   3. مسار الصفحة اللى عايزينه يروحها

//    كلمه لوجين <=   المشترك بينهم
Route::group(['prefix' => 'tests' , 'middleware'=>[]], function (){
    Route::get('/upload-file', 'HolderController@getUploadFileView');  //ودناه على الصفحة اللى عايزينه يروحها
    Route::post('/upload-file', 'HolderController@doUploading');  //ودناه على الصفحة اللى عايزينه يروحها
});






// Middleware OK
// Naming Routes OK
// Upload Images OK
// Controller (Update / Delete)
// API  ok
// Dashboard


