<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title','125'); //العنوان يكون 125 حرف
            $table->text('content');
            $table->unsignedBigInteger('image_id')->nullable(true);
            $table->timestamps();

            $table->foreign('image_id')   //عملنا ربط بين image_id  فى جدول الصور
            ->references('id')       //باال id  فى جدول sliders
            ->on('images')
                ->onUpdate('CASCADE')
                ->onDelete('SET NULL');


        });
    }
























    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}








