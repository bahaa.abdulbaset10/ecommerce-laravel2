<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100)->nullable(false)->unique();
            //عملنا حقل زيادة لل image_id وعملنا الديفولت بتاعه تبقى فاضية
            $table->unsignedBigInteger('image_id')->nullable(true);
            $table->text('description')->nullable(true);
            $table->timestamps();


            $table->foreign('image_id')   //عملنا ربط بين image_id  فى جدول الصور
            ->references('id')       //باال id  فى جدول اليوزرز
            ->on('images')
                ->onUpdate('CASCADE')
                ->onDelete('SET NULL');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
