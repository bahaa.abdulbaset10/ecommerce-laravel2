<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('first_user_id')->nullable(true); //الراسل الاول id
            $table->unsignedBigInteger('second_user_id')->nullable(true);//id الراسل التانى
            $table->boolean('is_seen')->default(false);      // هل الراسلة اتافت ولا لاء وعملنا الدفولت بفولس
            $table->boolean('is_forward')->default(true);   // كده الاول بعتها للتانى لو بفولس يبقى التانى بعتها للاول
            $table->text('message')->nullable(true);   //محتوى الرسالة ويكون الطبيعى بتاعه فاضى
            $table->timestamps();

            $table->foreign('first_user_id')  // //عملنا ربط بين first_user_id  فى جدول الشاتس
                ->references('id')              //باال id  فى جدول اليوزرز
                ->on('users')
                ->onUpdate('CASCADE')
                ->onDelete('SET NULL');

            $table->foreign('second_user_id') // //عملنا ربط بين second_user_id  فى جدول الشاتس
                ->references('id')             //باال id  فى جدول اليوزرز
                ->on('users')
                ->onUpdate('CASCADE')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
    }
}
