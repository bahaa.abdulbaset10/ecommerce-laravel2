<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Chat::class, function (Faker $faker) {

    $UserIDS = \App\user::pluck('id')->toArray(); // [10, 20, 30]
    $firstIndex = array_rand($UserIDS); //  الراسل الاول
    $secondIndex = array_rand($UserIDS); // الراسل التانى

    return [
        'first_user_id' =>$UserIDS[$firstIndex],  // ارجع بالراسل الاول
        'second_user_id' =>$UserIDS[$secondIndex],  //ارجع بالراسل التانى
        'message' => $faker->sentence(20), // الرسالة 20 كلمه
        'is_seen' => $faker->boolean(), //السين ياترو يافولس
        'is_forward' => $faker->boolean(), // الارسال ياترو يافولس

    ];
});
