<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {

    $UserIDS = \App\user::pluck('id')->toArray(); // [10, 20, 30]
    $index = array_rand($UserIDS); // جبنا ال ـid  بتاع اليوزر


    return [
        'user_id' =>$UserIDS[$index], // جبنا ال ـid  بتاع اليوزر
        'first_name' => $faker->firstName, //الاسم الاول
        'last_name' => $faker->lastName,  //الاسم التانى
        'phone_number' => $faker->phoneNumber, // رقم التموبايل
        'address' => $faker->address, // العنوان
        'discount' => $faker->randomFloat(2,0,100),  //اعمل  علمتين عشريين وبدايتى زيرو واخرى 100
        'tax' => $faker->randomFloat(2,0,1000),  //اعمل  علمتين عشريين وبدايتى زيرو واخرى 1000
        'is_received' => $faker->boolean(), //الاستلام
        'is_checked_out' => true,          //خلص ولا لاء
        'shipped_at' => $faker->dateTime(),  //ميعاد الشحن اللى هتوصل للعميل امتى
       ];
});

















