<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OrderDetail;
use Faker\Generator as Faker;


$factory->define(OrderDetail::class, function (Faker $faker) {


    $orderIDS = \App\Order::pluck('id')->toArray(); // [10, 20, 30]
    $productIDS = \App\Product::pluck('id')->toArray(); // [10, 20, 30]


    $index = array_rand($orderIDS); // جبنا ال ـid  الاوردر
    $index2 = array_rand($productIDS); // جبنا ال ـid  بتاع المنتج



    return [
        'order_id' =>$orderIDS[$index], // جبنا ال ـid  بتاع الاوردر
        'product_id' =>$productIDS[$index2], // جبنا ال ـid  بتاع المنتج
        'amount' =>$faker->randomNumber(),
        'price'  =>$faker->randomFloat(2,20,1000),
        'discount'  =>$faker->randomFloat(2,0,100),
    ];
});













