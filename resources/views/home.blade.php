@extends('commen.master')
@section('title')
    Home
@endsection
@section('content')
    @include('commen.navbar')
    <!--/******************************************************(2)**************************************************************/-->


    <div class="jumbotron jumbotron-fluid bg-light my-0 ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-center">
                        <h4 class="display-4">
                            E-Commerce System
                        </h4>
                        <p class="text-muted">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A alias delectus ducimus ea earum
                            eius eligendi error est expedita harum illum impedit in ipsum laboriosam laborum magnam,
                            modi officia officiis quos, sit, tenetur vitae voluptas voluptate? A possimus quidem
                            voluptatem.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row">           <!--------------------------------start  slide showe---------->


        <div class="col-12  col-md-12 col-lg-12">
            <div class="bd-example">
                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">

                    <ol class="carousel-indicators mb-2">

                        {{--عملنا لوب على الاسليدرز اللى عملناه فى الكنترولر--}}
                        {{--حرف ال k دا هو الاندكس--}}
                        {{--ودى علشان نربط عدد الصور بالشرط اللى تحتها بالاندكس--}}
                        @foreach($sliders as $k => $slider)
                            {{--هنا بنقول لو الاندكس بيساوى زيرو اعملها اكتف غير كده سيبها فاضيه علشان ميفتحتش الشورط اللى تحت الصورة كلها مع بعضها--}}
                            <li data-target="#carouselExampleCaptions" data-slide-to="{{$k}}" class="{{$k == 0 ? "active" : ""}}"></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        {{--عملنا لوب على الاسليدرز اللى عملناه فى الكنترولر--}}
                        {{--حرف ال k دا هو الاندكس--}}
                        @foreach($sliders as $k => $slider)
                            {{--هنا بنقول لو الاندكس بيساوى زيرو اعملها اكتف غير كده سيبها فاضيه علشان ميفتحتش الصور كلها مع بعضها--}}
                            {{--لو فى صورة هات المسار بتاعها لو مفيش صورة خليها null علشان ميحصلش ايرور--}}

                            <div class="carousel-item {{$k == 0 ? "active" : ""}}">
                                {{--مسار الصورة هات اول صورة من الفانيكشن اللى عملنها فى المودل اللى اسمها imageوالصورة لها مسار--}}
                                {{--لو فى صورة هاتها لو مفيش صورة اعمل null--}}
                                <img src="{{$slider->image ?  $slider->image->path : null}}"
                                     class="d-block w-100"
                                     alt="{{$slider->title}}"   {{--هات العنوان من جدول الاسليدر من الدتا بيس--}}
                                     height="350px" width="100%">
                                <div class="carousel-caption d-none d-md-block">
                                    <div class="card">
                                        <div class="card-body text-dark">
                                            <h5 class="h2">{{$slider->title}}</h5>
                                            <p>{{$slider->content}}</p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

                                   <!--------------------------------end  slide showe------------>




    <!--------------------------------------------------------------------------------------------------->
    <div class="container mt-5 mb-3">
        <div class="row">

            <div class="col-12 col-lg-3 ">
                <div class="row">
                    <div class="col-12">

                        <div class="card shadow">
                            <div class="card-body">
                                <h5 class="card-title font-weight-bold text-center">
                                    Categories
                                </h5>
                                <hr>

                                <!--------------------------------أقدم 10 منتجات على الجنب الشمال من صفحة الهوم -------------------------------------->
                                <ul class="list-unstyled text-dark mb-1">
                                    {{--عملنا لوب يلف على اقسام المنتاجت علشان يجبلنا اقدم 10--}}
                                    @foreach($cats as $cat)
                                        <li class="text-truncate d-block">
                                            <a href="/categories/{{$cat->id}}" {{--علشان لما نضفط على العنصر يودينى على صفحته من الـid بتاعه--}}
                                            style="outline: none; color: black; text-decoration: none;">
                                                <span class="fa fa-caret-right"></span>
                                                <span>{{$cat->name}}</span>   {{--علشان يعرضلنا اسم المنتج --}}

                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                                <!--------------------------------------------------------------------------------------------------->

                                <div class="row mt-2">
                                    <div class="col-12">
                                        <div class="text-left mb-4 ">
                                            <span class="fa fa-caret-right text-danger"></span>
                                            <a href="/products/top " class="text-danger"
                                               style="outline: none; text-decoration:none; color: #721c24">
                                                See More
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <h5 class="card-title text-center font-weight-bold">Popular Tags</h5>
                                <hr>
                                <span class="badge badge-primary badge-pill">Kids</span>
                                <span class="badge badge-primary badge-pill">Kids</span>
                                <span class="badge badge-primary badge-pill">Kids</span>
                                <span class="badge badge-primary badge-pill">Kids</span>

                                <br>
                                <span class="badge badge-primary badge-primary">Kids</span>
                                <span class="badge badge-primary badge-pill">Kids</span>
                                <span class="badge badge-primary badge-pill">Kids</span>
                                <span class="badge badge-primary badge-pill">Kids</span>
                                <span class="badge badge-primary badge-pill">Kids</span>

                                <br>
                                <span class="badge badge-primary badge-pill">Kids</span>
                                <span class="badge badge-primary badge-pill">Kids</span>
                                <span class="badge badge-primary badge-pill">Kids</span>
                                <span class="badge badge-primary badge-pill">Kids</span>
                                <span class="badge badge-primary badge-pill">Kids</span>

                                <br>
                                <span class="badge badge-primary badge-pill">Kids</span>
                                <span class="badge badge-primary badge-pill">Kids</span>
                                <span class="badge badge-primary badge-pill">Kids</span>
                                <span class="badge badge-primary badge-pill">Kids</span>
                                <span class="badge badge-primary badge-pill">Kids</span>

                            </div>

                        </div>
                    </div>
                </div>
            </div>




            <!--------------------------------------------------------------------------------------------------->

            <div class="col-12 col-lg-9 mt-4 mt-lg-0">

                {{--<div class="row">--}}
                {{--<div class="col-12">--}}
                {{--<div class=" mb-4 bg-light">--}}
                {{--<div class="">--}}
                {{--<input type="search" class="text-center form-control" placeholder="Search"--}}
                {{--style="width: 100%; margin: 0; padding: 0">--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}


                <div class="row mb-2">
                    <div class="col-12">
                        <div class="card shadow">
                            <div class="card-body">
                                <p class="m-0 text-center font-weight-bold h5">
                                    New Arrivals
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <!--------------------------------------المبيعات اللى وصلت حديثا فى صفحة الهوم------------------------------------->


                <div class="row">
                    @foreach($arrivals as $product)
                        @include('commen.product')      {{--هات ال 12 منتج--}}
                    @endforeach

                </div>

                <!--------------------------------------------------------------------------------------------------->

                <div class="row mt-2">
                    <div class="col-12">
                        <div class="text-center mb-4">
                            <a href="/products/arrivals" class="text-muted"
                               style="outline: none; text-decoration:none;">
                                See More Products
                            </a>
                        </div>
                    </div>
                </div>






            </div>


            {{--<<!----------------------------------------------------------------------------------------------------------->>--}}

            <div class="col-12  mt-4 mt-lg-0">
                {{--<div class="row">--}}
                {{--<div class="col-12">--}}
                {{--<div class=" mb-4 bg-light">--}}
                {{--<div class="">--}}
                {{--<input type="search" class="text-center form-control" placeholder="Search"--}}
                {{--style="width: 100%; margin: 0; padding: 0">--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}


                <div class="row mb-2">
                    <div class="col-12">
                        <div class="card shadow">
                            <div class="card-body">
                                <p class="m-0 text-center font-weight-bold h5">
                                    Top Sales
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <!--------------------------------- المننتجات الااكتر مبيعات فى صفحة الهوم------------------------------------->
                <div class="row">
                    @foreach($sales  as $product)
                        @include('commen.product')      {{--هات ال 12 منتج--}}
                    @endforeach

                </div>
                <!------------------------------------------------------------------------------->



                <div class="row mt-2">
                    <div class="col-12">
                        <div class="text-center mb-4">
                            <a href="/products/top" class="text-muted"
                               style="outline: none; text-decoration:none;">
                                See More  Top Sales
                            </a>
                        </div>
                    </div>
                </div>



            </div>



        </div>
    </div>

    <!--/******************************************************(11)**************************************************************/-->



    @include('commen.footer')
@endsection


