@if($errors->any())           {{--لو فى اى ايرور موجود اعرض الكارت الموجود والكلام اللى فيه مفيش ايرور متعرضش--}}
<div class="card shadow mb-5">
    <div class="card-body">
        <ul class="list-unstyled mb-0">
            @foreach($errors->all() as $error)    {{--اعمل لوب على  الايرورز وهات كل الايرورات الموجودة عندك --}}
            <li class="text-danger">
                <i class="fa fa fa-caret-right"></i>
                {{$error}}                    {{--اعرض الايرور --}}
            </li>
            @endforeach                             {{--قفلة الللوب --}}
        </ul>
    </div>
</div>
@endif                                      {{--قفلة ال if --}}