
<!--/******************************************************(1(navbar))**************************************************************/-->


<div class="jumbotron-fluid">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="/">
            <img src="images/logo.png" width="65px;" height="40px;" class=" img-fluid">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Categories
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @php
                            //هات اقدم 10 منتجات موجودين فى جدول المنتجات
                        $cats = \App\Category::orderBy('created_at','ASC')->take(10)->get();
                        @endphp

                        @foreach($cats as $cat)   {{--عملنا لوب على المنتجات--}}
                            <a class="dropdown-item" href="/categories/{{$cat->id}}">   {{--ودينى على الاى دى بتاع المنتج الموجود فى صفحة المنتجات --}}
                                {{$cat->name}}    {{--حطلى اسم المنتج هنا --}}
                            </a>
                        @endforeach

                        <a class="dropdown-item" href="/categories/">See More</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link  " href="/Recommendations" tabindex="-1" aria-disabled="true">Recommendations</a>
                </li>
            </ul>

            <ul class="navbar-nav mx-auto">
                <form action="/search" method="post" >
                    @csrf
                    <li class="form-inline">
                        <input type="text" class="form-control" name="search">
                        <button class="btn btn-success ml-2">
                            <i class="fa fa-search"></i>
                            Search
                        </button>
                    </li>
                </form>
            </ul>
            <ul class="navbar-nav ml-auto">
                @if(\Auth::check())                                 {{--اعمل اتشك على اللوجن لو المستخدم عامل لوجن--}}
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button"
                       data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <span>{{\Auth::user()->first_name}}</span>       {{--ادخل على جدول اليوزر هات من الحقل بتاع first_nameالاسم  اللى المستخدم داخل بيه--}}
                        <span>{{\Auth::user()->last_name}}</span>         {{--ادخل على جدول اليوزر هات من الحقل بتاع last_name الاسم  اللى المستخدم داخل بيه--}}
                    </a>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="navbarDropdown1">        {{--اعمل قائمة من العناصر فيها --}}

                        <a class="dropdown-item" href="/profile">Profile</a>        {{--البروفايل ولو المستخدم ضغط عليها توديه لصفحة البروفايل--}}
                        <a class="dropdown-item" href="/shopping-cart">My Cart</a>    {{--عنصر ماى كارت لو ضغط عليها توديك لصفحة التسوق--}}
                        <div class="dropdown-divider"></div>                           {{--اعمل خط بين العنصرين اللى فوق والعنصر اللى تحت--}}
                        <a class="dropdown-item" href="/logout">Logout</a>             {{--زرار لوج اوت لو ضغط عليها تخرجك لصفحة لوج اوت--}}
                    </div>
                </li>
                @else           {{--لو المستخدم مش عامل لوجين --}}
                <li class="nav-item">
                    <a class="nav-link" href="/login">Login</a>        {{--خلى الزرار مكتوب عليه لوج ان  ودخله لصفحة اللوجين --}}
                </li>
                @endif
            </ul>
        </div>
    </nav>
</div>

<!--/******************************************************(1(navbar))**************************************************************/-->

