


<!--/******************************************************(footer)**************************************************************/-->

<div class="jumbotron-fluid color-font-labny">

    <div class="container">


        <div class="row pt-5 pb-5">
            <div class="col-12 col-md-6 col-lg-3 mt-3">
                <div class="card border-0">
                    <div class="card-body bg-info">
                        <h5 class="color_white">Quick Links</h5>

                        <hr style="background-color: #fff;">
                        <ul class="list-unstyled ml-2">
                            <li class="d-block"><a href="/about-us" class="text-decoration-none li-hover"><span class="fa fa-caret-right color_white"></span><span class="color_white"> About Us</span></a></li>
                            <li class="d-block"><a href="D:/corces/SELECT CORSE/html+css+bootstrap/project 1/contact-us.html" class="text-decoration-none li-hover"><span class="fa fa-caret-right color_white"></span><span class="color_white"> Contact Us</span></a></li>
                            <li class="d-block"><a href="/about-us" class="text-decoration-none li-hover"><span class="fa fa-caret-right color_white"></span><span class="color_white"> FAQ</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="col-12 col-md-6 col-lg-3 mt-3">
                <div class="card border-0">
                    <div class="card-body bg-info">
                        <h5 class="color_white">Top Searches</h5>

                        <hr style="background-color: #fff;">
                        <ul class="list-unstyled ml-2">
                            <li class="d-block"><a href="/about-us" class="text-decoration-none li-hover"><span class="fa fa-caret-right color_white"></span><span class="color_white"> Labore blanditiis tempor</span></a></li>
                            <li class="d-block"><a href="/about-us" class="text-decoration-none li-hover"><span class="fa fa-caret-right color_white"></span><span class="color_white"> Aut sed excepturi nesciunt </span></a></li>
                            <li class="d-block"><a href="/about-us" class="text-decoration-none li-hover"><span class="fa fa-caret-right color_white"></span><span class="color_white"> Error consequatur repellat</span></a></li>
                            <li class="d-block"><a href="/about-us" class="text-decoration-none li-hover"><span class="fa fa-caret-right color_white"></span><span class="color_white"> Atque odio fugit dicta </span></a></li>
                            <li class="d-block"><a href="/about-us" class="text-decoration-none li-hover"><span class="fa fa-caret-right color_white"></span><span class="color_white"> Accusamus libero commod</span></a></li>

                        </ul>
                    </div>
                </div>
            </div>


            <div class="col-12 col-md-6 col-lg-3 mt-3">
                <div class="card border-0">
                    <div class="card-body bg-info">
                        <h5 class="color_white">Contact Us</h5>

                        <hr style="background-color: #fff;">
                        <ul class="list-unstyled ml-2">
                            <li class="d-block"><span class="fa fa-address-card color_white"></span><span class="color_white"> Labore blanditiis tempor</span></li>
                            <li class="d-block"><span class="fa fa-phone-square color_white"></span><span class="color_white"> (000) 0000-0000-000</span></li>
                            <li class="d-block"><span class="fa fa-phone-volume color_white"></span><span class="color_white"> (000) 0000-0000-000</span></li>
                            <li class="d-block"><span class="fa fa-at color_white"></span><span class="color_white"> lorem@ipsum.dolor.com</span></li>

                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-3 mt-3">
                <div class="card border-0">
                    <div class="card-body bg-info">
                        <h5 class="color_white">Follow Us</h5>

                        <hr style="background-color: #fff;">
                        <ul class="list-unstyled ml-2">
                            <a href="#" class="mr-1 color_white"><span class="fab fa-facebook fa-2x color_white"></span></a>
                            <a href="#" class="mr-1 color_white"><span class="fab fa-twitter fa-2x color_white"></span></a>
                            <a href="#" class="mr-1 color_white"><span class="fab fa-linkedin fa-2x color_white"></span></a>
                            <a href="#" class="mr-1 color_white"><span class="fab fa-google-plus fa-2x color_white"></span></a>
                            <a href="#" class="mr-1 color_white"><span class="fab fa-pinterest fa-2x color_white"></span></a>
                            <a href="#" class="mr-1 color_white"><span class="fab fa-instagram fa-2x color_white"></span></a>
                            <a href="#" class="mr-1 color_white"><span class="fab fa-youtube fa-2x color_white"></span></a>
                        </ul>



                        <h5 class="color_white pt-2">Quick Links</h5>

                        <hr style="background-color: #fff;">
                        <ul class="list-unstyled ml-2">
                            <a href="#" class="mr-1 color_white"><span class="fab fa-google-play fa-2x color_white"></span></a>
                            <a href="#" class="mr-1 color_white"><span class="fab fa-app-store-ios fa-2x color_white"></span></a>
                        </ul>

                    </div>
                </div>
            </div>

        </div>




        <div class="row">
            <div class="col-12 text-center">
                <span class="color_white">© All rights are reserved.</span>
            </div>
        </div>
    </div>
</div>



<!--/******************************************************(footer)**************************************************************/-->

