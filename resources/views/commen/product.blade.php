

<div class="col-12 col-md-4  mb-3">
    <div class="card shadow">
        {{--مسار الصورة هات اول صورة من الفانيكشن اللى عملنها فى المودل اللى اسمها imageوالصورة لها مسار--}}
        {{--لو فى صورة هاتها لو مفيش صورة اعمل null--}}
        <img src="{{$product->image ?  $product->image->path : null}}"
             class="card-img-top" height="175">
        <div class="card-body">
            <h5 class="card-title text-center font-weight-bold text-truncate m-0 mb-2">
                {{$product->name}}  {{--اسم المنتج من حق الـname فى جدول المنتجات --}}
                </h5>
            <div class="mb-2" style="width: 100%; text-align: center;">
                <span class="badge badge-pill badge-info border-0 text-truncate d-block py-1">

                    {{--هات اسم القسم اللى فيه هذا المنتج من الفانيكشن اللى اسمها category اللى عاملنها فى البروفايدر بتاع الـ product --}}
                    {{$product->category? $product->category->name : "No Category"}}
                </span>
            </div>
            <div class="mb-2" style="width: 100%; text-align: center;">         {{--هات سعر البيع  اللى فى جدول المنتجات--}}
                <span class="badge badge-pill badge-primary border-0">price : {{$product->selling_price}}</span>
                <span class="badge badge-pill badge-danger border-0" >discount : {{$product->discount}}</span>
            </div>                                                                      {{--هات حق الخصم من جدول المنتجات--}}

            <p class="text-center m-0 text-muted font-italic text-truncate mb-1">
            {{$product->description}}  {{--هات الوصف من حقل الـ description اللى فى جدول المنتجات--}}
            <div style="text-align: center;">
                <form action="" method="post">
                    @csrf
                    {{--عملنا input دا مخفى علشان اما اعمل اما اضغط على الزرار يودينى للاى دى بتاع المنتج يحولنى على الصفحة بتاعته --}}
                    <input type="hidden" name="product" value="{{$product->id}}">
                    <button type="submit" class="btn btn-success btn-block btn-sm">Add to cart</button>
                </form>

                <a href="#" class="text-muted" style="font-size: 14px; outline: none; text-decoration: none;">
                    display info
                </a>
            </div>
        </div>
    </div>
</div>