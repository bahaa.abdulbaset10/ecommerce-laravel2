@extends('commen.master')

@section('title')
    My Shopping Cart
@endsection


@section('content')

    @include('commen.navbar')
    <!--/******************************************************(2)**************************************************************/-->


    <div class="jumbotron jumbotron-fluid bg-light my-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-center">
                        <h4 class="display-4">
                            My Shopping Cart
                        </h4>
                        <p class="text-muted">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A alias delectus ducimus ea earum
                            eius eligendi error est expedita harum illum impedit in ipsum laboriosam laborum magnam,
                            modi officia officiis quos, sit, tenetur vitae voluptas voluptate? A possimus quidem
                            voluptatem.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--/******************************************************(2)**************************************************************/-->


    <!-------------------------------------------------------------------------------------------------------------------------------ـ/-->

    <div class="container">
        <div class="row my-4">
            <div class="col-12">
                <div class="card shadow">
                    @include('commen.success')
                    @include('commen.errors')
                    @if(count($lastOrderDetails) > 0)   {{--لو عدد اخر اوردر اللى فى الـ CartController اكبر من الصفر --}}

                    <div class="card-body mb-0">        {{--اعرض الاورد بتفاصيله --}}
                        <table class="table-bordered table table-hover table-bordered mb-0">
                                <thead>
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Discount</th>
                                    <th>Amount</th>
                                    <th>Overall</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                            <tbody>
                            @php $overallprice = 0; @endphp    {{--عملنا متغير ببيساوى صفر علشان نجيب اجمالى الاوردر كله --}}
                                 {{--عملنا لوب على اخر اوردر اللى عملناه فى الكنترولر--}}
                            @foreach($lastOrderDetails  as $k => $item) {{--حرف ال k دا هو الاندكس--}}
                                @php $product = $item->product @endphp  {{--هات الاى دى بتاع المنتج --}}
                                @php      //معادلة اجمالى السعر
                                     //معادلة الاجمالى جبنا السعر وضربناه فى الكمية وضربناه فى فرق الخصم  (الخصم ناقص واحد) وبنقسم على ميه علشان يقربه لاقرب رقم عشرى
                                       $overall = $item->price * $item->amount * (1- $item->discount /100);
                                       $overall = ceil($overall * 100) / 100;   //عملنا الحركة دى علشان اقربه لاقرب علمتين عشريتين فقط
                                       $overallprice +=  $overall;  //فى كل مره لف على اجمالى سعر المنتج وجمعهم وكلهم فى اجمالى الفاتورة
                                @endphp
                                <tr class="text-center">
                                    <td>{{$k + 1}}</td>  {{--علشان يبدأ من زيرو وليس واحد--}}
                                    <td width="35%">{{$product->name}}</td> {{--جبت الاسم من المودل بتاع orderdeatils--}}
                                    <td>{{$item->price}}</td> {{--جبت السعر من معايا لان ممكن يكون اللى هناك اتعدل واللى معايا متعدلش--}}
                                    <td>{{$item->discount}}%</td>{{--جبت الخصم من معايا لان ممكن يكون اللى هناك اتعدل واللى معايا متعدلش--}}
                                    <td>{{$item->amount}}</td>{{--جبت الكمية من معايا لان ممكن يكون اللى هناك اتعدل واللى معايا متعدلش--}}
                                    <td>{{$overall}}</td>{{--جبت الاجمالى من المعادلة اللى عملناها فوق --}}
                                    <td>


                        <a href="/products/{{$item->product_id}}/details"class="text-decoration-none btn btn-info btn-sm mb-1">More Details</a>
                                        {{--حولنى لتفاصيل المنتج اللى انا مختارة --}}
                                        <form action="/shopping-cart/delete" method="POST" id="removeForm">
                                            @csrf
                                            {{--عملنا input دا مخفى علشان اما اعمل اما اضغط على الزرار يودينى للاى دى بتاع المنتج اللى انا عايز امسحه --}}
                                            <input type="hidden" name="product" value="{{$product->id}}">
                                            <button id="removeBtn"
                                                    type="submit" class="btn btn-danger btn-sm">
                                                Remove
                                            </button></form></td></tr>
                            @endforeach
                            <tr class="text-center">
                                <td colspan="5">Overall Price</td>
                                {{$overallprice}}</td>  {{--جبنا اجمالى الفاتورة كلها من المعادلة الى عملنها فوق overallprice  --}}
                                <td class="font-weight-bold" colspan="2">
                            </tr>
                            </tbody>
                        </table>
                        <div class="text-center mt-3 form-inline">
                            <button class="btn btn-success mr-2 ml-auto">
                                Checkout Order
                            </button>
                            <form action="/shopping-cart/delete-all"
                                  class="mr-auto"
                                  method="POST" id="removeAllForm">
                                @csrf
                                <button class="btn btn-danger" id="removeAllBtn">
                                    Cancel Order
                                </button></form></div></div>

                    @else                               {{--لو عدد اخر اوردر اللى فى الـ CartController اقل من الصفر يعنى مفيش اوردر --}}
                    <div class="alert alert-warning mb-0" role="alert">       {{--اعرضلة الرسالة دى انت لاتمتلك منتجات فى كارت التسوق الحالى --}}
                        You have no product currently in your shopping cart!
                    </div>
                    @endif
                </div></div></div></div>

    <!-------------------------------------------------------------------------------------------------------------------------------ـ/-->





    @include('commen.footer')
@endsection


@section('more-script')

    <script>
        $(document).ready(function () { {{--استنى اما البتاع يحمل كله --}}
         $('#removeBtn').click(function (ev) {  {{--هات الـ removeBtn الاى دى بتاع مفتاح مسح عنصر من الاوردر --}}
          ev.preventDefault();  {{---هتجيب الحدث بتاع المفتاح وتخليه ديفولت علشان ميعملش submit--}}
          var isConfirmed = confirm("Are you sure?");{{--وتساله هل انت متاكد من المسح --}}
           if(isConfirmed){  //لو متاكد ان هيمسح
               $('#removeForm').submit(); //هجيب الاى دى بتاع الـ form واقوله اعمل submit
        }

        });


            $('#removeAllBtn').click(function (ev) {  {{--هات الـ removeAllBtn الاى دى بتاع مفتاح مسح الاوردر كله --}}
            ev.preventDefault();  {{---هتجيب الحدث بتاع المفتاح وتخليه ديفولت علشان ميعملش submit--}}
            var isConfirmed = confirm("Are you sure to cancel the order?");{{--وتساله هل انت متاكد من الغاء الاوردر كله  --}}
            if(isConfirmed){  //لو متاكد ان هيلغى
                $('#removeAllForm').submit(); //هجيب الاى دى بتاع الـ form واقوله اعمل submit
            }

            });
        });
    </script>
@endsection