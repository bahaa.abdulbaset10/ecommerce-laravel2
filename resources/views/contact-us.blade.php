@extends('commen.master')

@section('title')
    Contact Us
@endsection


@section('content')

    @include('commen.navbar')
    <!--/********************************************************************************************************************/-->


    <div class="jumbotron-fluid mt-0 color-font-labny2 " style="height: 600px">

        <div class="container ">
            <div class="row" style="padding-top: 150px; padding-bottom: 150px;">


                <!--##############[1]#################-->
                <div class="col-12 col-md-12 col-lg-12 pt-5 pb-5 pl-0 ">
                    <h1 class="display-4  text-center  h1-font"> E-Commerce Recommendation System</h1>

                    <p class="text-muted text-center ">exercitationem explicabo facere hic magnam maiores quasi quibusdam,
                        sapiente sit suscipit ut vero voluptatibus.</p>

                    <nav aria-label="breadcrumb" class="w-auto d-flex justify-content-center">
                        <ol class="breadcrumb bg-transparent mx-auto w-auto">
                            <li class="breadcrumb-item text-center">
                                <a href="/">Home</a>
                            </li>
                            <li class="breadcrumb-item text-center active" aria-current="page">
                                Contact Us
                            </li>
                        </ol>
                    </nav>
                </div>


            </div>

        </div>
    </div>
    </div>

    <div class="container">
        <div class="row ">
            <div class="col-12 ">
                <div class="text-center">
                    <h2>Find Us</h2>
                    <p class="text-muted">You can find us (the headquarters) using Google Map</p>
                    <hr>
                    @include('commen.success')     {{--عملنا استدعاء الى صفحة ال success --}}
                </div>
            </div>
        </div>
    </div>


    <div class="container">

        <div class="col-12">
            <div class="card border card-hover">
                <div class="card-body">
                    <div id="googleMap" class="w-100" style="height: 400px; position: relative; overflow: hidden;">
                        <div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);">
                            <div class="gm-err-container">
                                <div class="gm-err-content">
                                    <div class="gm-err-icon"><img
                                                src="https://maps.gstatic.com/mapfiles/api-3/images/icon_error.png"
                                                draggable="false" style="user-select: none;"></div>
                                    <div class="gm-err-title">Oops! Something went wrong.</div>
                                    <div class="gm-err-message">This page didn't load Google Maps correctly. See the
                                        JavaScript console for technical details.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row ">
            <div class="col-12 ">
                <div class="text-center">
                    <h2>Find Us</h2>
                    <p class="text-muted">You can find us (the headquarters) using Google Map</p>
                    <hr>
                </div>
            </div>
        </div>
    </div>





    <div class="container ">
        <div class="text-center">

            <div class="card mb-3 text-center " style="max-width: 100%;">

                <div class="row mt-2 p-4">
                    <div class="col-12 order-last order-md-first col-md-4">
                        <div class="row mt-3">
                            <div class="col-12">
                                <p class="text-center f0475e">
                                    <i class="fa fa-search-location fa-3x color_labny"></i>
                                </p>
                                <p class="text-center h4">
                                    Location
                                </p>
                                <p class="text-center h6 text-muted">
                                    Lorem ipsum dolor sit amet, consectetur. Eiusmod tempor incididunt.
                                </p>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-12">
                                <p class="text-center f0475e">
                                    <i class="fa fa-phone fa-3x color_labny"></i>
                                </p>
                                <p class="text-center h4">
                                    Help Center
                                </p>
                                <p class="text-center h6 text-muted">
                                    (000) 0000-0000-000
                                </p>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-12">
                                <p class="text-center f0475e">
                                    <i class="fa fa-at fa-3x color_labny"></i>
                                </p>
                                <p class="text-center h4">
                                    Email
                                </p>
                                <p class="text-center h6 text-muted">
                                    ....@....com
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-8">

                        @include('commen.errors')     {{--عملنا استدعاء الى صفحة الايرورز --}}

                        <div class="card border card-hover">
                            <div class="card-body">

                                <form action="" method="POST" novalidate>
                                    @csrf  {{--يؤجل شرحها دلوقت--}}

                                    <div class="form-group row">
                                        <label for="username" class="col-form-label col-12 col-md-3 font-weight-bold">Name</label>
                                        <div class="col-12 col-md-9">
                                            <input type="text" class="form-control" name="name"  value="{{old('name')}}">
                                                   {{--هات القيمة القديمة اللى كانت موجودة فى التكست بوكس من قبل  اللى كانت قبل الايرورو--}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-form-label col-12 col-md-3 font-weight-bold">Email</label>
                                        <div class="col-12 col-md-9">

                                            <input type="email" class="form-control" name="email" value="{{old('email')}}">
                                    </div>        {{--هات القيمة القديمة اللى كانت موجودة فى التكست بوكس من قبل  اللى كانت قبل الايرورو--}}
                                    </div>
                                    <div class="form-group row">
                                        <label for="phone" class="col-form-label col-12 col-md-3 font-weight-bold">Phone</label>
                                        <div class="col-12 col-md-9">
                                            <input type="tel" class="form-control" name="phone" value="{{old('phone')}}">
                                                  {{--هات القيمة القديمة اللى كانت موجودة فى التكست بوكس من قبل  اللى كانت قبل الايرورو--}}
                                         </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="feedback" class="col-form-label col-12 col-md-3 font-weight-bold">Feedback</label>

                                        <div class="col-12 col-md-9">
                                    <textarea name="feedback" cols="30" rows="2"
                                          class="form-control"></textarea>
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <button class="btn btn-info">Send</button>
                                    </div>

                                    <div class="text-left">
                                        <p class="my-0 py-0 text-muted">
                                            All Fields are Required
                                        </p>
                                    </div>
                                </form>







                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <!--/*******************************************************************************************************************/-->


    @include('commen.footer')
@endsection


