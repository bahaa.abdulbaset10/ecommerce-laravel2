@extends('commen.master')

@section('title')
    Home
@endsection


@section('content')

    @include('commen.navbar')
    <!--/******************************************************(2)**************************************************************/-->

    <div class="jumbotron-fluid mt-0 color-font-labny2">
        <div class="container">
            <div class="row" style="padding-top: 150px; padding-bottom: 150px;">


                <!--##############[1]#################-->
                <div class="col-12 col-md-6 col-lg-6 pt-5 pb-5 pl-0 ">
                    <h1 class="display-4  text-center text-md-left h1-font"> E-Commerce Recommendation System</h1>

                    <p class="text-muted text-center text-md-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium animi asperiores, deserunt dolore exercitationem explicabo facere hic magnam maiores quasi quibusdam,  sapiente sit suscipit ut vero voluptatibus.</p>

                    <div class="badge-pill w-50 text-center d-none d-md-block  mt-4 mb-2 mx-auto btn-info">
                        <a href="/home" class="text-white btn btn-f0475e btn-block">
                            Start Surfing...
                        </a>
                    </div>
                </div>




                <!--##############[2]#################-->
                <div class="col-12 col-md-6 col-lg-6 p-0 pt-4 text-center ">
                    <img src="{{asset('images/lool2.png')}}" width="80%" height="80%" class=" img-fluid">
                </div>


            </div>

        </div>
    </div>
    <div class="container">


        <div class="row" style="margin-top: 200px">
            <div class="col-12 col-md-12 col-lg-12 text-center">

                <h2 class="h1-font"> E-Commerce Recommendation System</h2>

                <p class="text-muted text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque aut facilis fuga fugit voluptatum. A adipisci, aliquam reiciendis repellat asperiores consectetur, repellat reprehenderit velit vitae facere fugit illo laborum maxime quia reiciendis repellat reprehenderit velit vitae.</p>


                <div class="badge-pill  text-center mt-4 mb-2 mx-auto btn-success" style="width: 150px;">
                    <a href="/home" class="text-white btn btn-block">
                        Get Started
                    </a>
                </div>

            </div>

        </div>

        <!--/******************************************************(2)**************************************************************/-->






        <!--/******************************************************(3)**************************************************************/-->


        <div class="row"style="margin-top: 50px">

            <div class="col-12 col-md-12 col-lg-12 text-center">

                <img src="{{asset('images/lool1.png')}}" width="40%" height="40%" class=" img-fluid">
            </div>


        </div>

        <!--/******************************************************(3)**************************************************************/-->





    </div>


    <!--/******************************************************(4)**************************************************************/-->
    <div class="jumbotron-fluid mb-5 color-font-labny2 pb-5" style="margin-top: 150px;">


        <div class="container w-100 h-100">

            <div class="row">
                <div class="col-12 " style="margin-top: 10%;">

                    <h2 class="h1-font text-center font-weight-bold">Lorem ipsum dolor sit amet, consectetur.</h2>

                    <p class="text-muted text-center">Lorem ipsum dolor sit amet,
                        consectetur adipisicing elit. Aliquam amet autem commodi, consequatur, consequuntur cumque deleniti, dolores ea eum excepturi
                        exercitationem magni mollitia nemo rem vitae? Dolor eaque iure perspiciatis? Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Aliquam amet autem commodi, consequatur, consequuntur cumque deleniti, dolores ea eum excepturi exercitationem magni mollitia nemo rem
                        vitae? Dolor.</p>
                </div>

                <div class="badge-pill  text-center   mx-auto btn" style="background-color: #0a9746; width: 150px;">
                    <a href="/home" class="text-white btn btn-block ">
                        Get Started
                    </a>
                </div>

            </div>


        </div>


    </div>

    <!--/******************************************************(4)**************************************************************/-->




    <!--/******************************************************(5)**************************************************************/-->


    <div class="container">


        <div class="row" style="margin-top: 150px">
            <div class="col-12 col-md-12 col-lg-12 text-center">

                <h2 class="h1-font"> E-Commerce Recommendation System</h2>

                <p class="text-muted text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque aut facilis fuga fugit voluptatum. A adipisci, aliquam reiciendis repellat asperiores consectetur, repellat reprehenderit velit vitae facere fugit illo laborum maxime quia reiciendis repellat reprehenderit velit vitae.</p>


            </div>

        </div>

        <!--/******************************************************(5)**************************************************************/-->






        <!--/******************************************************(6)**************************************************************/-->

        <div class="row mb-5">

            <div class="col-12 col-md-4 mt-2">
                <div class="card card-hover">

                    <div class="card-body ">
                        <p class="text-center my-0">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>

                        <p class="text-center my-2  display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>

                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>

                    </div>
                </div>
            </div>





            <div class="col-12 col-md-4 mt-2">
                <div class="card card-hover">

                    <div class="card-body">
                        <p class="text-center my-0 f0475e">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>
                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>
                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>
                    </div>
                </div>
            </div>


            <div class="col-12 col-md-4 mt-2">
                <div class="card card-hover">

                    <div class="card-body">
                        <p class="text-center my-0 f0475e">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>
                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>
                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>
                    </div>
                </div>
            </div>


            <div class="col-12 col-md-4 mt-2">
                <div class="card card-hover">

                    <div class="card-body ">
                        <p class="text-center my-0">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>

                        <p class="text-center my-2  display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>

                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>

                    </div>
                </div>
            </div>





            <div class="col-12 col-md-4 mt-2">
                <div class="card card-hover">

                    <div class="card-body">
                        <p class="text-center my-0 f0475e">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>
                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>
                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>
                    </div>
                </div>
            </div>


            <div class="col-12 col-md-4 mt-2">
                <div class="card card-hover">

                    <div class="card-body">
                        <p class="text-center my-0 f0475e">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>
                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>
                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4 mt-2">
                <div class="card card-hover">

                    <div class="card-body ">
                        <p class="text-center my-0">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>

                        <p class="text-center my-2  display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>

                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>

                    </div>
                </div>
            </div>





            <div class="col-12 col-md-4 mt-2">
                <div class="card card-hover">

                    <div class="card-body">
                        <p class="text-center my-0 f0475e">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>
                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>
                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>
                    </div>
                </div>
            </div>


            <div class="col-12 col-md-4 mt-2">
                <div class="card card-hover">

                    <div class="card-body">
                        <p class="text-center my-0 f0475e">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>
                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>
                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>
                    </div>
                </div>
            </div>


            <div class="col-12 col-md-4 mt-2">
                <div class="card card-hover">

                    <div class="card-body ">
                        <p class="text-center my-0">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>

                        <p class="text-center my-2  display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>

                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>

                    </div>
                </div>
            </div>





            <div class="col-12 col-md-4 mt-2">
                <div class="card card-hover">

                    <div class="card-body">
                        <p class="text-center my-0 f0475e">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>
                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>
                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>
                    </div>
                </div>
            </div>


            <div class="col-12 col-md-4 mt-2">
                <div class="card card-hover">

                    <div class="card-body">
                        <p class="text-center my-0 f0475e">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>
                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>
                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>
                    </div>
                </div>
            </div>

        </div>

    </div>



    <!--/******************************************************(6)**************************************************************/-->




    <!--/******************************************************(7)**************************************************************/-->



    <div class="jumbotron-fluid mb-5 color-font-labny2 pb-5" style="margin-top: 150px;">


        <div class="container">


            <div class="row ">
                <div class="col-12 " style="margin-top: 10%;">

                    <h2 class="h1-font text-center font-weight-bold">Lorem ipsum dolor sit amet, consectetur.</h2>

                    <p class="text-muted text-center">Lorem ipsum dolor sit amet,
                        consectetur adipisicing elit. Aliquam amet autem commodi, consequatur, consequuntur cumque deleniti, dolores ea eum excepturi
                        exercitationem magni mollitia nemo rem vitae? Dolor eaque iure perspiciatis? Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Aliquam amet autem commodi, consequatur, consequuntur cumque deleniti, dolores ea eum excepturi exercitationem magni mollitia nemo rem
                        vitae? Dolor.</p>
                </div>

                <div class="row p-1 ">

                    <!--##############[1]#################-->

                    <div class="col-6 col-md-12 col-lg-12">

                        <div class="row">

                            <div class="col-12 col-md-2 col-lg-2 mb-2">
                                <div class="card card-hover text-center">
                                    <div class="card-body ">
                                        <p class="text-center my-0 f0475e">
                                            <i class="fa  fa-eye fa-3x color_labny2 text-center"></i>
                                        </p>
                                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                                            Lorem Ipsum
                                        </p>
                                    </div>
                                </div>
                            </div>



                            <div class="col-12 col-md-2 col-lg-2 mb-2">
                                <div class="card card-hover">
                                    <div class="card-body">
                                        <p class="text-center my-0 f0475e">
                                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                                        </p>
                                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                                            Lorem Ipsum
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-2 col-lg-2 mb-2">
                                <div class="card card-hover">
                                    <div class="card-body">
                                        <p class="text-center my-0 f0475e">
                                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                                        </p>
                                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                                            Lorem Ipsum
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-2 col-lg-2 mb-2">
                                <div class="card card-hover">
                                    <div class="card-body">
                                        <p class="text-center my-0 f0475e">
                                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                                        </p>
                                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                                            Lorem Ipsum
                                        </p>

                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-2 col-lg-2 mb-2">
                                <div class="card card-hover">
                                    <div class="card-body">
                                        <p class="text-center my-0 f0475e">
                                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                                        </p>
                                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                                            Lorem Ipsum
                                        </p>

                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-2 col-lg-2 mb-2">
                                <div class="card card-hover">
                                    <div class="card-body">
                                        <p class="text-center my-0 f0475e">
                                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                                        </p>
                                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                                            Lorem Ipsum
                                        </p>

                                    </div>
                                </div>
                            </div>



                        </div>


                    </div>

                    <!--##############[1]#################-->


                    <!--##############[2]#################-->

                    <div class="col-6 col-md-12 col-lg-12">

                        <div class="row">

                            <div class="col-12 col-md-2 col-lg-2 mb-2">
                                <div class="card card-hover">
                                    <div class="card-body">
                                        <p class="text-center my-0 f0475e">
                                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                                        </p>
                                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                                            Lorem Ipsum
                                        </p>
                                    </div>
                                </div>
                            </div>



                            <div class="col-12 col-md-2 col-lg-2 mb-2">
                                <div class="card card-hover">
                                    <div class="card-body">
                                        <p class="text-center my-0 f0475e">
                                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                                        </p>
                                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                                            Lorem Ipsum
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-2 col-lg-2 mb-2">
                                <div class="card card-hover">
                                    <div class="card-body">
                                        <p class="text-center my-0 f0475e">
                                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                                        </p>
                                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                                            Lorem Ipsum
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-2 col-lg-2 mb-2">
                                <div class="card card-hover">
                                    <div class="card-body">
                                        <p class="text-center my-0 f0475e">
                                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                                        </p>
                                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                                            Lorem Ipsum
                                        </p>

                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-2 col-lg-2 mb-2">
                                <div class="card card-hover">
                                    <div class="card-body">
                                        <p class="text-center my-0 f0475e">
                                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                                        </p>
                                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                                            Lorem Ipsum
                                        </p>

                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-2 col-lg-2 mb-2">
                                <div class="card card-hover">
                                    <div class="card-body">
                                        <p class="text-center my-0 f0475e">
                                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                                        </p>
                                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                                            Lorem Ipsum
                                        </p>

                                    </div>
                                </div>
                            </div>



                        </div>


                    </div>

                    <!--##############[2]#################-->

                </div>

            </div>

        </div>

    </div>




    <!--/******************************************************(7)**************************************************************/-->






    <!--/******************************************************(8)**************************************************************/-->


    <div class="container">


        <div class="row" style="margin-top: 150px">
            <div class="col-12 col-md-12 col-lg-12 text-center">

                <h2 class="h1-font"> E-Commerce Recommendation System</h2>

                <p class="text-muted text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque aut facilis fuga fugit voluptatum. A adipisci, aliquam reiciendis repellat asperiores consectetur, repellat reprehenderit velit vitae facere fugit illo laborum maxime quia reiciendis repellat reprehenderit velit vitae.</p>


            </div>

        </div>




        <div class="row">

            <div class="col-12 col-md-3 mt-3">
                <div class="card card-hover">
                    <div class="card-body">
                        <p class="text-center my-0 f0475e">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>
                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>
                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>
                    </div>
                </div>
            </div>


            <div class="col-12 col-md-3 mt-3">
                <div class="card card-hover">
                    <div class="card-body">
                        <p class="text-center my-0 f0475e">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>
                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>
                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-3 mt-3">
                <div class="card card-hover">
                    <div class="card-body">
                        <p class="text-center my-0 f0475e">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>
                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>
                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>
                    </div>
                </div>
            </div>


            <div class="col-12 col-md-3 mt-3">
                <div class="card card-hover">
                    <div class="card-body">
                        <p class="text-center my-0 f0475e">
                            <i class="fa  fa-eye fa-3x color_labny2"></i>
                        </p>
                        <p class="text-center my-2 f0475e display-7 font-weight-bold color_labny">
                            Lorem Ipsum
                        </p>
                        <p class="text-center text-muted my-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit
                        </p>
                    </div>
                </div>
            </div>





        </div>











    </div>



    <!--/******************************************************(8)**************************************************************/-->



    <!--/******************************************************(9)**************************************************************/-->
    <div class="jumbotron-fluid mb-5 color-font-labny2 pb-5" style="margin-top: 150px;">

        <div class="container">

            <div class="row pt-5">

                <div class="col-12 col-md-8 col-lg-8 ">

                    <div class="col-12 col-md-12 col-lg-12 text-left ">
                        <div class="text-center text-md-left text-lg-left">

                            <h2 class="h1-font"> E-Commerce Recommendation System</h2>

                            <p class="text-muted text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Atque aut facilis fuga fugit voluptatum. A adipisci, aliquam reiciendis repellat asperiores
                                consectetur, repellat reprehenderit velit vitae facere fugit illo laborum maxime quia reiciendis
                                repellat reprehenderit velit vitae.</p>
                            <div class="text-center text-md-left text-lg-left mb-5">
                                <button class="btn btn-outline-success my-2 my-sm-0  " type="submit">Login
                                </button>
                            </div>


                        </div>
                    </div>

                </div>

                <div class="col-12 col-md-4 col-lg-4  text-sm-center">
                    <img src="{{asset('images/lool2.png')}}" width="100%" height="100%" class=" img-fluid">
                </div>

            </div>
        </div>
    </div>


    <!--/******************************************************(9)**************************************************************/-->







    <!--/******************************************************(10	)**************************************************************/-->

    <div class="container" style="margin-bottom: 150px;">


        <div class="row">
            <div class="col-12 " style="margin-top: 10%;">

                <h2 class="h1-font text-center font-weight-bold">Lorem ipsum dolor sit amet, consectetur.</h2>

                <p class="text-muted text-center">Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Aliquam amet autem commodi, consequatur, consequuntur cumque deleniti, dolores ea eum excepturi
                    exercitationem magni mollitia nemo rem vitae? Dolor eaque iure perspiciatis? Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                </p>
            </div>
        </div>
        <!--/******************************************************(10)**************************************************************/-->





        <!--/******************************************************(11)**************************************************************/-->

        <div class="container">


            <div class="row ">

                <div class="col-12 col-md-3 col-lg-3 ">


                </div>
                <div class="col-12 col-md-6 col-lg-6 card-hover "style=" border: 1px solid #26a8df;">

                    <div class="input-group mb-3 mt-3">
                        <input type="text" class="form-control" placeholder="Enter your email address..."
                               aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-success" type="button" id="button-addon2">Button</button>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-md-3 col-lg-3 ">


                </div>
            </div>
        </div>
    </div>
    <!--/******************************************************(11)**************************************************************/-->


    @include('commen.footer')
@endsection


