@extends('commen.master')

@section('title')
    Register
@endsection


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8 mx-auto mt-4">
                <div class="card shadow">
                    <div class="card-body">

                        @include('commen.errors')     {{--عملنا استدعاء الى صفحة الايرورز --}}

                        <form action="" method="POST">
                            @csrf
                            <div class="form-group row">
                                <label for="firstName"
                                       class="col-form-label col-12 col-md-3">First Name *</label>
                                <div class="col-12 col-md-9">
                                    <input type="text"
                                           value="{{old('firstName')}}"  {{--هات القيمة القديمة اللى كانت موجودة فى التكست بوكس من قبل --}}
                                           class="form-control" name="firstName">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lastName" class="col-form-label col-12 col-md-3">Last Name</label>
                                <div class="col-12 col-md-9">
                                    <input type="text"
                                           value="{{old('lastName')}}"  {{--هات القيمة القديمة اللى كانت موجودة فى التكست بوكس من قبل --}}
                                           class="form-control" name="lastName">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="username" class="col-form-label col-12 col-md-3">Username *</label>
                                <div class="col-12 col-md-9">
                                    <input type="text"
                                           value="{{old('username')}}"  {{--هات القيمة القديمة اللى كانت موجودة فى التكست بوكس من قبل --}}
                                           class="form-control" name="username">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-form-label col-12 col-md-3">Email *</label>
                                <div class="col-12 col-md-9">
                                    <input type="text"
                                           value="{{old('email')}}"   {{--هات القيمة القديمة اللى كانت موجودة فى التكست بوكس من قبل --}}
                                           class="form-control" name="email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-form-label col-12 col-md-3">Password *</label>
                                <div class="col-12 col-md-9">
                                    <input type="password" class="form-control" name="password">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="gender" class="col-form-label col-12 col-md-3">Gender</label>
                                <div class="col-12 col-md-9">


                                    @php
                                        $genders = \App\Gender::all();   //عملنا متغير نسخة من جدول الجندر الموجود فى الدتا بيس
                                    @endphp

                                    <select name="gender" class="form-control">

                                        @foreach($genders as $gender)            {{--عملنا لوب يلف على كل العناصر الموجودة داخل جدول الجندر وسميها جندر --}}
                                             @if($gender->id == old('gender'))    {{--لو id بتاع العنصر اللى عليه الدور بيساوى العنصر اللى اخترته قبل كده --}}
                                                  <option value="{{$gender->id}}" selected> {{--هات الاى دى بتاعه حطه فى الفاليو بتاعت الاوبشن وخلية سيلكتد --}}
                                                    {{$gender->name}}                     {{--واعرض اسمه --}}
                                                  </option>
                                             @else                                           {{--طب لو لاء  --}}
                                                <option value="{{$gender->id}}">
                                                  {{$gender->name}}
                                                </option>                                 {{--سيبها من غير سيلكتد على طبيعتها --}}
                                             @endif
                                        @endforeach
                                    </select>


                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phoneNumber" class="col-form-label col-12 col-md-3">Phone Number</label>
                                <div class="col-12 col-md-9">
                                    <input type="text"
                                           value="{{old('phoneNumber')}}"     {{--هات القيمة القديمة اللى كانت موجودة فى التكست بوكس من قبل --}}
                                           class="form-control" name="phoneNumber">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address" class="col-form-label col-12 col-md-3">Address</label>
                                <div class="col-12 col-md-9">
                                    <input type="text"
                                           value="{{old('address')}}"        {{--هات القيمة القديمة اللى كانت موجودة فى التكست بوكس من قبل --}}
                                           class="form-control" name="address">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="bio" class="col-form-label col-12 col-md-3">Bio</label>
                                <div class="col-12 col-md-9">
                                    <textarea name="bio" id="bio" cols="30"
                                              rows="3" class="form-control">{{old('bio')}}</textarea>   {{--هات القيمة القديمة اللى كانت موجودة فى التكست بوكس من قبل --}}
                                </div>
                            </div>

                            <div class="text-center mb-2">
                                <button class="btn btn-info" type="submit">
                                    Register
                                </button>
                            </div>
                            <p class="m-0">
                                <span class="text-muted">Having an account ?!</span>
                                <a href="/login">Login</a>
                            </p>
                            <p class="m-0">
                                <span class="text-muted">Return</span>
                                <a href="/">Home</a>
                            </p>
                            <p class="text-muted">(*) Required Fields</p>  {{--النجمة اللى هنا دى بنحطها هنا وقدام كل ليبل علشان نعرفة ان الحقل دا مهم ومطلوب --}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection