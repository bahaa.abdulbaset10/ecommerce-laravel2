@extends('commen.master')

@section('title')
    login
@endsection


@section('content')

    @include('commen.navbar')

    <!--/******************************************************(login)**************************************************************/-->


    <div class="jumbotron-fluid color-font-labny2" style="height: 753px;">


        <div class="container  text-center">

            <div class="row text-center">

                <div class="col-12 text-center " style="height: 753px; padding-top: 10%;">

                    <div class="">

                        <div class="row ">
                            <div class="col-12 text-center">
                                <a href="index.html"><img src="images/logo.png" width="10%" height="10%"
                                                          class=" img-fluid"></a>
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-12 text-center">
                                <p class="color_labny">E-Commerce Recommendation System</p>
                            </div>
                        </div>


                        <div class="w-75 h-75 m-auto pt-2  ">
                            <div class="card   w-75 pt-5  m-auto card-hover">
                                <div class="card-body ">

                                   @include('commen.errors')     {{--عملنا استدعاء الى صفحة الايرورز --}}
                                   @include('commen.success')     {{--عملنا استدعاء الى صفحة ال success --}}


                                    <form action="/login" method="POST">
                                        @csrf

                                        <div class="form-group row">
                                            <label for="username" class="col-form-label col-12 col-lg-3 text-lg-right">
                                                Username
                                            </label>
                                            <div class="col-12 col-md-9">
                                                <input type="text" class="form-control"
                                                       value="{{old('username')}}"    {{--هات القيمة القديمة اللى كانت موجودة فى التكست بوكس من قبل --}}
                                                       name="username">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="password" class="col-form-label col-12 col-lg-3 text-lg-right">
                                                Password
                                            </label>
                                            <div class="col-12 col-lg-9">
                                                <input type="password" id="password" name="password"
                                                       class="form-control">
                                            </div>
                                        </div>

                                        <div class="text-center text-lg-right">
                                            <button class="btn btn-info pill badge-pill" style="width:30%;">Login
                                            </button>
                                        </div>

                                        <div class="text-left mt-2">
                                            <p class="my-0 text-muted">
                                                Does not Having an Account?
                                                <a href="/register">Register</a>
                                            </p>
                                            <p class="my-0 text-muted">
                                                All Fields are Required
                                            </p>
                                        </div>
                                    </form>




                                </div>


                            </div>

                            <div class="row text-center">
                                <ul class="col-12 h-list text-default">
                                    <li><a href="https://egypt.souq.com/eg-ar/terms-and-conditions/c/"
                                           id="auth-portal-condition" local="">Conditions of Use</a></li>
                                    <li><a href="https://egypt.souq.com/eg-ar/privacy-policy/c/"
                                           id="auth-portal-privacy" local="">Privacy Notice</a></li>
                                    <li><a href="https://egypt.souq.com/eg-ar/HelpCenter/Home" id="auth-portal-help"
                                           local=""> Help</a></li>
                                </ul>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12 text-center">
                                <span class="">© All rights are reserved.</span>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
    </div>










    <!--/******************************************************(login)**************************************************************/-->


    @include('commen.footer')
@endsection


